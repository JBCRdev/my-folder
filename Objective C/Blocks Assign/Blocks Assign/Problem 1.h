//
//  Problem 1.h
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-08.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Problem_1 : NSObject
@property NSMutableArray<NSString*>*(^func5)(NSMutableArray<int(^)(void)>*);
@end

NS_ASSUME_NONNULL_END
