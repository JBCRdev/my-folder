//
//  ViewController.m
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-08.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import "ViewController.h"
#import "closure.h"
#import "Problem 3.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //  problem 3
    double(^app)(int,int) = [Problem_3 CalculatorNum1:32 Num2:23 operation:@"SUM"];
    double r = app(32,23);
    NSLog(@"problem 3:");
    printf("%lf", r);
    
    // problem 4
    bool intPredicate = [closure intPredicarNumber:82 predicate:^BOOL (int x){
        return x>100;
    }];
    NSLog(@"problem 4.1:");
    NSLog(@"%d",intPredicate);
   
    
    double bifunctional = [closure biFunctionNum1:10 num2:54.23 function:^(int x, double z) {
        return x+z;
    }];
    NSLog(@"problem 4.2:");
    printf("%lf", bifunctional);
    
    [closure consumerString:@"Hello, Josue" consumer:^(NSString* str){
    NSLog(@"problem 4.3:");
    NSLog(@"%@",str);
    }];
    
    NSArray *numbers = [closure functionN1:@1 n2:@2 n3:@3 n4:@4 n5:@5 func:^(NSNumber* a, NSNumber* b, NSNumber* c, NSNumber* d, NSNumber* e){
        
        NSArray *r = [NSArray arrayWithObjects:a,b,c,d,e, nil];
        return r;
    }];
    int count=0;
    while (count < [numbers count]){
        NSLog(@"problem 4.4:");
        NSLog(@"%@", numbers[count]);
        count++;
    }
    
    double supplyer = [closure DoubleSupplyer:^(void){return 25.5;}];
    NSLog(@"problem 4.5:");
  printf("%lf", supplyer);
   
}
@end
