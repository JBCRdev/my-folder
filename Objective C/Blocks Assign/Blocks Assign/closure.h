//
//  closure.h
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-09.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface closure : NSObject
+ (bool)intPredicarNumber:(int)number predicate:(bool(^)(int))testpred;
+(double)biFunctionNum1:(int)num1 num2:(double)num2 function:(double(^)(int,double))apply;
+ (void)consumerString:(NSString*)ToPrint consumer:(void(^)(NSString*))accept;
+ (NSArray*)functionN1:(NSNumber*)n1 n2:(NSNumber*)n2 n3:(NSNumber*)n3 n4:(NSNumber*)n4 n5:(NSNumber*)n5 func:(NSArray*(^)(NSNumber*,NSNumber*,NSNumber*,NSNumber*,NSNumber*))apply;
+ (double)DoubleSupplyer:(double(^)(void))supply;
@end

NS_ASSUME_NONNULL_END
