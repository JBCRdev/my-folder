//
//  AppDelegate.h
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-08.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

