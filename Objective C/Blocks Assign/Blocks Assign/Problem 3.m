//
//  Problem 3.m
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-08.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import "Problem 3.h"


@implementation Problem_3

 double(^sum)(int,int)=^(int num1, int num2){
    double result = num1 + num2;
    return result;
 };
 double(^divition)(int,int)=^(int num1, int num2){
    double result = num1/num2;
    return result;
 };
double(^multiplication)(int,int)=^(int num1, int num2) {
    double result = num1*num2;
    return result;
};
double(^difference)(int,int)=^(int num1, int num2) {
    double result = num1-num2;
    return result;
};
double(^power)(int,int)=^(int num1, int num2) {
    
    double result = pow(num1, num2);
    return result;
};
+ (NSDictionary<NSString*,double(^)(int,int)>*)createDic{
    
    NSDictionary<NSString*,double(^)(int,int)>* operation = [@{@"SUM":sum,@"DIVITION":divition,@"MULTIPLICATION":multiplication,@"DIFFERENCE":difference,@"POWER":power} mutableCopy];
    
    return operation;
}

+ (double(^)(int,int))CalculatorNum1:(int)num1 Num2:(int)num2 operation:(NSString*)operation {
    NSDictionary<NSString*,double(^)(int,int)>* dicc = [self createDic];
    id formula = dicc[operation];
    
    if (formula != nil){
        return formula;
        
    }else {printf("%s", "sorry operation didn't find");}
    
    return nil;
}

@end


