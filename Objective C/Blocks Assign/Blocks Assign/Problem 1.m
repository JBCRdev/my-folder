//
//  Problem 1.m
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-08.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import "Problem 1.h"

@implementation Problem_1

int (^func1)(int,int)=^(int a, int b) {
    int result = a+b;
    return result;
};
void (^func2)(NSString*)=^(NSString* str){
    printf("%s", [str UTF8String]);
};
int (^func3)(void)=^(void){
    int x = 10;
    return x;
};
NSMutableArray<void(^)(void)>*(^func4)(void) = ^(void) {
    NSMutableArray *myarray = [[NSMutableArray alloc]init];
    for (int i = 0; i<5; i++){
        void(^new)(void);
        [myarray addObject:new];
    }
    return myarray;
};

NSMutableArray<NSString*>*(^func5)(NSMutableArray<int(^)(void)>*)=^(NSMutableArray<int(^)(void)>* blocks){
    NSMutableArray<NSString*>* strings = [[NSMutableArray<NSString*> alloc] init];
   
    int count = 0;
    while (count < [blocks count]){
        int x = blocks[count]();
        NSString *str = [NSString stringWithFormat:@"%d", x];
        [strings addObject:str];
        count++;
    }
   
    return strings;
};
NSMutableArray<int(^)(void)>*(^func6)(NSMutableArray<int(^)(void)>*)=^(NSMutableArray<int(^)(void)>* blocks) {
    
    NSMutableArray<int(^)(void)>* newArray = [[NSMutableArray<int(^)(void)>alloc]init];
    
    newArray = blocks;
    
    return newArray;
};

@end
