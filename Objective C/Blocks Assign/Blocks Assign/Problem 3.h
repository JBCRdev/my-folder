//
//  Problem 3.h
//  Blocks Assign
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-10-08.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Problem_3 : NSObject
+ (NSDictionary<NSString*,double(^)(int,int)>*)createDic;
+ (double(^)(int,int))CalculatorNum1:(int)num1 Num2:(int)num2 operation:(NSString*)operation;
@end

NS_ASSUME_NONNULL_END
