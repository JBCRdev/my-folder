//
//  main.m
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PatternProvider.h"
#import "ServiceRequester.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        
    }
    
}
