//
//  AppDelegate.h
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

