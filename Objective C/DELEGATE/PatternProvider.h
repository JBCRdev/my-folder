//
//  PatternProvider.h
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UIKit/UIKit.h"

@protocol PatternPProtocol <NSObject>

- (char)wichCharacter;
- (int)howManyLines;
- (NSString *)whichPattern;
- (void)setDeligatee;

@optional
- (int)getspaceChar;
- (int)getspaceLine;

@end

@interface PatternProvider : NSObject
@property (nonatomic, weak) id<PatternPProtocol> delegate;
- (void) getStuff;
@end


