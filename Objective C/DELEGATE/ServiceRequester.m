
//
//  ServiceRequester.m
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import "ServiceRequester.h"
#import "PatternProvider.h"


@interface ServiceRequester()

@property (nonatomic) char charac;
@property (nonatomic) int lines;
@property (strong, nonatomic) NSString* pattern;

@end

@implementation ServiceRequester

- (instancetype)initTheCharac:(char)character lines:(int)numOfLines pat:(NSString*)pattern;
{
    if ((self = [super init])) {
        _charac = character;
        _lines = numOfLines;
        _pattern = pattern;
        _pp = nil;
    }
    return self;
}

- (char)wichCharacter{
    return self.charac;
}
- (int)howManyLines{
    return self.lines;
}
- (NSString *)whichPattern{
    return self.pattern;
}
- (int)getspaceChar{
    return 0;
}

- (void)setDeligatee {
    if (self.pp){
        self.pp.delegate = self;
        
    }
}



@end
