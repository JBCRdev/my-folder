//
//  ServiceRequester.h
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PatternProvider.h"


@interface ServiceRequester : NSObject<PatternPProtocol>
@property (strong, nonatomic)  PatternProvider* pp;

- (instancetype)initTheCharac:(char)character lines:(int)numOfLines pat:(NSString*)pattern;
@end
