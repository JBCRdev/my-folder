//
//  PatternProvider.m
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import "PatternProvider.h"
#import "ServiceRequester.h"
@interface PatternProvider()

@property NSString* spaceLine;
@property NSString*  spaceChar;
@end

@implementation PatternProvider

-(id)init
{
    if ((self = [super init]))
    {
        _spaceLine = @" ";
        _spaceChar = @"";
        _delegate = nil;
    }
    return self;
}

- (void)getStuff{
    if (_delegate) {
        
        char character = [_delegate wichCharacter];
        int lines = [_delegate howManyLines];
        NSString * pattern = [_delegate whichPattern];
        NSString * sL = [self getspaceLine];
        NSString * sC = [self getpaceChar];
        
        [self drowChar:character lines:lines pat:pattern sC:sC sL:sL];
    }
}
- (NSString *)getspaceLine{
    if ([self.delegate respondsToSelector:@selector(getspaceChar)]){
        int x = [self.delegate getspaceChar];
        switch (x) {
            case 0:
                return @"";
                break;
            case 1:
                return @" ";
                break;
            default:
                return @"  ";
                break;
        }
    } else {
        return self.spaceChar;
    }
}

- (NSString *)getpaceChar{
    if ([self.delegate respondsToSelector:@selector(getspaceLine)]) {
        int x = [self. delegate getspaceLine];
        switch (x) {
            case 0:
                return @"";
                break;
            default:
                return @" ";
                break;
        }
    } else {
        return self.spaceLine;
    }
}

- (void)drowChar:(char)character lines:(int)numLines pat:(NSString *)pattern sC:(NSString *)sC sL:(NSString *)sL{
    if ([pattern isEqualToString: @"TRIANGULE"]){
        if (numLines != 0){
            for (int i=0 ; i<numLines; i++) {
                printf("%c%s",character,[sC UTF8String]);
            }
            NSLog(@"%@", sL);
            [self drowChar:character lines:numLines-1 pat:pattern sC:sC sL:sL];
        } else{return;}
        
    } else if ([pattern isEqualToString:@"REVERSETRIANGULE"]){
        if (numLines != 0) {
            [self drowChar:character lines:numLines-1 pat:pattern sC:sC sL:sL];
            for (int i=0; i<numLines; i++) {
                printf("%c%s",character,[sC UTF8String]);
            }
            NSLog(@"%@", sL);
        } else {return;}
    }
    else {NSLog(@"A correct pattern is needed");
        return;
    }
}
@end
