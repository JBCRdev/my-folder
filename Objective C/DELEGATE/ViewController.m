//
//  ViewController.m
//  DELEGATE
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-09-28.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#import "ViewController.h"
#import "ServiceRequester.h"
#import "PatternProvider.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    PatternProvider* painter = [[PatternProvider alloc] init];
    ServiceRequester* costomer = [[ServiceRequester alloc]initTheCharac:'a' lines:6 pat:@"TRIANGULE"];
    costomer.pp = painter;
    costomer.pp.delegate = costomer;
    [painter getStuff];
}


@end
