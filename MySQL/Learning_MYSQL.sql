-- SELECT *, quantity * unit_price AS "total price"
-- FROM sql_store.order_items
-- WHERE order_id = 6 AND (quantity * unit_price) > 30
-- WHERE order_id = 2
-- ORDER BY "total price" DESC

SELECT order_id, oi.product_id, p.name, quantity, oi.unit_price
FROM order_items oi
JOIN products p
	ON oi.product_id = p.product_id