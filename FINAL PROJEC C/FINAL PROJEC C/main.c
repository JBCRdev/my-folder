//
//  main.c
//  FINAL PROJEC C
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-05-31.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct edge
{
    char desti;
    int value;
    char from;
    struct edge* next;
}edge;

typedef struct node
{
    struct node* neighbour;
    char name;
    int value;
    char from;
    int visited;
    struct edge* edge;
}node;

//VARIABLE DECLARATION
int getNumberOfNodes (void);
int getNumberOfEdges (node* head);
char* getNodes (int nod);
void getEdges(node* head, int ned);
node* nodeMaker (char* arrNod, int nod);
void insideLink (node* head, char f, char t, int v);
char getDestination (node* head);
char getSource (node* head);
void manager ( node* head, int nod, int ned, char source,char dest);
void valueAssigner (node* head, char source, int nod);
void label (node* head, char source, int nod);
void nextNode (node* head, char source, int nod);
void valueUpdater (node* head, edge* changer, char source);
int printing (char* print, int sum, char dest, int d);
void prePrint (char source, char dest, node* head, int nod);
int main (void)
{
    int nod= getNumberOfNodes();
    char* arrNod=getNodes(nod);
    node* head=nodeMaker(arrNod,nod);
    int ned=getNumberOfEdges(head);
    getEdges(head,ned);
    char source=getSource(head);
    char dest=getDestination(head);
    label(head,source,nod);
    valueAssigner(head, source, nod);
    
    manager(head,nod, ned,source, dest);
}

void manager ( node* head, int nod, int ned, char source,char dest)
{
    int number=0;
    
    while (number != nod)
    {
        nextNode(head, source, nod);
        number=number+1;
    }
    
    
    prePrint(source, dest, head, nod);
}
int getNumberOfNodes (void)
{
    int nod=0;
    printf("Enter the number of Nodes:\n ");
    scanf("%d",&nod);
    
    return nod;
}
int getNumberOfEdges (node* head)
{
    int ned=0;
    printf("Enter the number of edges:\n ");
    scanf("%d",&ned);
    
    return ned;
}
char* getNodes (int nod)
{
    char nodes[nod];
    int i= 0;
    int a=0;
    printf("Enter the Nodes (like this: A,B,C...):\n");
    scanf("%s",nodes);
    
    char* arrNod=(char*)malloc(sizeof(char)*5);
    
    while (nodes[i] != '\0')
    {
        if (nodes[i]== ',')
        {
            i= i+1;
        }
        
        arrNod[a]=nodes[i];
        i= i+1;
        a= a+1;
    }
    
    
    return arrNod;
}

char getSource (node* head)
{
    char source;
    
    printf("What is the source?\n");
    scanf("%s",&source);
    
    return source;
}
char getDestination (node* head)
{
    char dest;
    
    printf("What is your destination:\n?");
    scanf("%s",&dest);
    
    return dest;
}

node* nodeMaker (char* arrNod, int nod)
{
    node* act=NULL;
    node* head=NULL;
    node* new= NULL;
    
    // head creation
    act=(node*)malloc(sizeof(node));
    head=(node*)malloc(sizeof(node));
    
    act->neighbour=NULL;
    act->from='\0';
    act->name=arrNod[0];
    act->value=0;
    act->visited=0;
    act->edge=NULL;
    
    //printf("head=")
    
    head=act;
    
    // other nodes (change loop dependin of the nodes)
    int i=0;
    
    while (arrNod[i] !='\0')
    {
        new=(node*)malloc(sizeof(node));
        
        new->neighbour=NULL;
        new->from='\0';
        new->name=arrNod[i+1];
        new->value=0;
        new->visited=0;
        new->edge=NULL;
        
        
        act->neighbour=new;
        act=new;
        //act->neighbour=NULL;
        
        i=i+1;
    }
    
    
    return head;
}


void getEdges(node* head, int ned)
{
    int sum=0;
    while (sum<ned)
    {
        printf("-Enter the Edge #%d-\n",sum+1);
        
        
        char f = '\0';
        printf("From:\n");
        scanf(" %c", &f);
        
        
        char t= '\0';
        printf("To:");
        scanf(" %c", &t);
        
        int v ;
        printf("Value:");
        scanf("%d", &v);
        
        
        insideLink(head,f,t,v);
        sum=sum+1;
    }
    
    
}

void insideLink (node* head, char f, char t, int v)
{
    node* connect=NULL;
    //edge* newone=NULL;
    edge* first=NULL;
    //newone=(edge*)malloc(sizeof(edge));
    connect=(node*)malloc(sizeof(node));
    connect=head;
    
    while (connect->name !=f)
    {
        connect=connect->neighbour;
    }
    
    if (connect->edge==NULL)
    {
        first=(edge*)malloc(sizeof(edge));
        connect->edge=first;
        first->from=f;
        first->desti=t;
        first->value=v;
        first->next=NULL;
    }
    else
    {
        edge* temp=connect->edge;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        
        first=(edge*)malloc(sizeof(edge));
        temp->next=first;
        first->from=f;
        first->desti=t;
        first->value=v;
        first->next=NULL;
        
    }
    
    
    
}

void valueAssigner (node* head, char source, int nod)
{
    node* set=NULL;
    
    set=head;
    
    for (int i=0; i<nod;i++)
    {
        if (set->name==source)
        {
            set->value=0;
        }
        else
        {
            set->value=32000;
        }
        
        set=set->neighbour;
    }
}

void label (node* head, char source, int nod)
{
    node* set =NULL;
    set=head;
    
    for (int i=0; i<nod;i++)
    {
        if (set->name==source)
        {
            set->from=source;
        }
        else
        {
            set->from='X';
        }
        set=set->neighbour;
    }
}

void nextNode (node* head, char source, int nod)
{
    edge* changer=NULL;
    node* point =NULL;
    changer=(edge*)malloc(sizeof(edge));
    point=(node*)malloc(sizeof(node));
    int sum=0;
    
    point=head;
    
    for (int i=0; i<nod;i++)
    {
        if (point->visited==1)
        {
            sum=sum+1;
        }
        point=point->neighbour;
    }
    
    point=head;
    if (sum==0)
    {
        
        while (point->name != source)
        {
            point=point->neighbour;
        }
        point->visited=1;
        changer=point->edge;
        valueUpdater(head, changer,source);
    }
    else
    {
        point=head;
        int min=1000;
        
        for (int i=0; i<nod;i++)
        {
            if (point->value < min && point->visited== 0)
            {
                min=point->value;
                point=point->neighbour;
            }
            else
            {
                point=point->neighbour;
            }
        }
        
        point=head;
        while (point->value != min)
        {
            point=point->neighbour;
        }
        changer=point->edge;
        point->visited=1;
        valueUpdater(head, changer,source);
    }
}

void valueUpdater (node* head, edge* changer, char source)

{
    node* newpoint=NULL;
    newpoint=head;
    
    
    while (changer !=NULL)
    {
        while (newpoint->name!=changer->desti||newpoint->visited ==1)
        {
            newpoint=newpoint->neighbour;
            if (newpoint->neighbour==NULL)
            {
                break;
            }
        }
        if (changer->value < newpoint->value)
        {
            newpoint->value= changer->value;
            newpoint->from=changer->from;
            
        }
        changer=changer->next;
        newpoint=head;
    }
}

void prePrint (char source, char dest, node* head, int nod)
{
    
    int sum=0;
    char print[nod];
    node* finder=NULL;
    //finder=(node*)malloc(sizeof(node));
    node* second=NULL;
    second=(node*)malloc(sizeof(node));
    
    
    print[0]=source;
    
    int d=0;
    finder=head;
    
    while (finder->name !=dest )
    {
        finder=finder->neighbour;
    }
    
    if (finder->from==source)
    {
        print[nod-1]=finder->name;
        sum=sum+finder->value;
        
        printing(print, sum, dest, d);
    }
    
    else
    {
        print[nod-1]=finder->name;
        sum=sum+finder->value;
        second=finder;
        
        finder=head;
        
        int d=1;
        while (second->from != source)
        {
            
            while (finder->name != second->from)
            {
                finder=finder->neighbour;
            }
            print[d]=finder->name;
            sum=sum+finder->value;
            second=finder;
            finder=head;
            d = d+1;
        }
        printing(print,sum, dest, d);
    }
}


int printing (char* print, int sum, char dest, int d)
{
    printf("This is the shortest path:\t");
    printf("%c->",print[0]);
    
    
    while (d !=0)
    {
        {
            printf("%c->",print[d]);
            d=d-1;
        }
    }
    
    printf("%c->",dest);
    printf("|%d.\n",sum);
    
    return 0;
}


