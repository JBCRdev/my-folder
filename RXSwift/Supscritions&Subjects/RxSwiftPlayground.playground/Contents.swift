//: Please build the scheme 'RxSwiftPlayground' first
import RxSwift
import Foundation

example(of: "creating observable"){
  
  /// Explisit declaration of observable with .just
  /// - Note: .just is only to observe one element.
  let mostPopular: Observable<String> = Observable<String>.just(episodeV)
  
  /// Implisit declaration of observable with .of
  /// - Note: .of is for multiple elements but not array.
  let originalTrilogy = Observable.of(episodeIV,episodeV,episodeVI)
  
  /// Implisit declaration of observable with .from
  /// - Note: .from is for array of element
  let prequelTrilogy = Observable.from([episodeI,episodeII,episodeIII])
}


 /// Subscribing to Observables

example(of: "subscribe"){
  let observable = Observable.of(episodeIV,episodeV,episodeVI)

  observable.subscribe{event in
    print(event)
  }
  
  /// Accesing to element property

  print("\nAccesing to element property:\n")
  observable.subscribe{event in
    print(event.element ?? event)
  }
  
  //Accesing to only onNext events
  // Notice how only the next events are print an not the completed
  
  print("\nAccesing to only onNext events:\n")
  observable.subscribe(onNext: {element in
    print(element)
  })
}

/// Example of creating an empty observable (no elemnts)
/// - Note: This observable is only to observe completed events

example(of: "empty"){
  let observable = Observable<Void>.empty()
  
  observable.subscribe(onNext:{element in
    print(element)
  }, onCompleted: {print("completed")
    
  })

}

/// Example of creating an empty at all observable (never operator contains nothing)
/// - Note: Never operator can be useful to represent an infinite duration

example(of: "never"){
  let observable = Observable<Any>.never()
  
  observable.subscribe(onNext: {element in
    print(element)
  }, onCompleted: {
    print("completed")
  })
  
}

/// Using the return value to cancel the subscriotion
/// - Note: When we call dispose on the subscription we cancelled ones is finish or terminated


example(of: "dispose"){
  let mostPopular = Observable.of(episodeV,episodeIV,episodeVI)
  
  let subscription = mostPopular.subscribe{event in
    print(event)
  }
  
  subscription.dispose()
}

/// Creating a dispose bag to remove the subscription before deallocation
/// - Note: To the end we call disposed(by:) to add the susbscription to a disposeBag

example(of: "DisposeBag"){
  let disposeBag = DisposeBag()
  
  Observable.of(episodeVII,episodeI,rogueOne)
    .subscribe{
      print($0)
  }
  .disposed(by: disposeBag)
}



// ------------ CREATING AN OBSERVABLE WITH .create Function --------------

example(of: "create"){
  
  enum Droid: Error {
    case OUB12
  }
  
  let disposeBag = DisposeBag()
  
  Observable<String>.create { observer in
    
    observer.onNext("R2-D2")
    /// When this error is read the observable finish and is disposed
    observer.onError(Droid.OUB12)
    observer.onNext("C-3PO")
    observer.onNext("K-2SO")
    observer.onCompleted()
    
    return Disposables.create()
  }
    .subscribe(onNext: { print($0)},
               onError: {print("Error:", $0)},
               onCompleted: {print("Completed")},
               onDisposed: {print("Disposed")})
  .disposed(by: disposeBag)
}

/// - Important: If you dont add an error, a compleated event and a dispose you'll be on a liky memory stade


// -------------------------  TRAITS --------------------

/// Traits are design to provide more contex to your code.
/*
 Traits:
 
 Single: one next event or error event
 
 Completable: completed event or error event
 
 Maybe: one next, completed or error event
*/

example(of: "Single"){
  enum FileReadError: Error {
    case fileNotFound, unreadable, encodingFailed
  }
  
  let disposeBag = DisposeBag()
  
  /// 1.- Create a func which its output in a Single<String> observer
  
  func loadText(from filename: String)->Single<String>{
    
    /** - Note: Single.create func have to return a single which at the same time have to be a disposable like
     ((SingleEvenet)->())-> Disposable
     **/
    return Single.create(subscribe: { (single) -> Disposable in
      let disposable = Disposables.create()
      /// 2.-  We get the FilePath, if an error occurs we return disposable and a Single with error event
      guard let path  = Bundle.main.path(forResource: filename, ofType: "txt") else {
        single(.error(FileReadError.fileNotFound))
        return disposable
      }
      /// 3.- Once we have the path we get the data, if an error occurs we return disposable and a single with an error
      guard let data = FileManager.default.contents(atPath: path) else {
        single(.error(FileReadError.unreadable))
        return disposable
      }
      /// 4.- We get the content from data, if an error occurs we return disposable and a single with an error
      guard let content = String(data: data, encoding: .utf8) else {
        single(.error(FileReadError.encodingFailed))
        return disposable
      }
      /// 5.-  If all goes well we return the single with the event an the disposable
      single(.success(content))
      return disposable
    })
  }
   /// Calling the function with the name of the file
  
  loadText(from: "ANewHope")
    .subscribe{ (SingleEvent) in
      /// Manage subscription for single event
      switch SingleEvent {
      case .error(let error):
        print("Error:", error)
      case .success(let string):
        print(string)
      }
      /// Disposing subscription once is finish
  }.disposed(by: disposeBag)
}


// ----------------------- SIDE EFFECTS ----------------------------

example(of: "never (SIDE EFFEC)"){
  let observable = Observable<Any>.never()
  let disposBag = DisposeBag()
  /**
   .do operator let you add side effects. These are handlers that let you do things that will not change the emitted event in anyway.
   .do will just pass the event through to the next operator in the chain.
 **/
  observable.do(onNext: {element in print("comming from do", element)},
                onError: {error in print("error comming from do", error)},
                onCompleted: { print("onCompleted from do")},
                onSubscribe: {print("onSubscribe from do")},
                onSubscribed: {print("onSubscribed from do")},
                onDispose: {print("onDispose from do")})
    .subscribe(onNext: {element in
      print(element)
    }, onCompleted: {
      print("completed")
    }).disposed(by: disposBag)
  
}



//------------------ SUBJECTS -------------------

/**
 Subjects are comprice in two parts
 
 - Observable: Can be subscribed to
 - Observer: Add new elements
 
 There are 3 types of subjects and one type called variable that wraps a subject
  1) PublishSubject
  2) BehaviourSubject
  3) ReplaySubject
  4) Variable
 
 PusblishSubjects
  - Starts empty
  - Emits new next events to new subscribers
 **/

// ------------- PUBLISHSUBJECT -----------------

example(of: "PusblishSubject"){
  
  let quote = PublishSubject<String>()
  /// Add a new event
  quote.onNext(itsNotMyFault)
  
  /// Subscribe to gett he events
  
   /// Beacuse the subscription is afeter the event post the subscriber does not get the print
  let subscriptionOne = quote.subscribe(){
    print("Subscription 1):", $0)
  }
  
  quote.onNext(doOrDoNot)

  let subscriptionTwo = quote.subscribe {
    print("Subscription 2):", $0)
  }
  /// In this case subsc1 gets "do or donot " ans "lack of faith"
  /// Subsc2 only gets Lack of Faith
  quote.onNext(lackOfFaith)
  
  subscriptionOne.dispose()
  
  quote.onNext(eyesCanDeceive)
  
  quote.onCompleted()
  
  let susbcriptionThree = quote.subscribe{
  print( "subscription 3) ",  $0)
  }
  
  quote.onNext(stayOnTarget)
  /// Disposing everything
  subscriptionTwo.dispose()
  susbcriptionThree.dispose()
}

///-------------- BehaviorSubjects ------------------

/**
 - Starts with initial values
 - Replays initial / lates value to new subscribers
 
  1 - 2 --- 3 --->|
    |
    1 - 2 - 3 --->|
          |
          2 - 3-->|
 **/

example(of: "BehaviourSubject"){
  let disposeBag = DisposeBag()
  /// BehaviourSubejcts allways start with a value
  let quotes = BehaviorSubject(value: iAmYourFather)
  
  /// this subscription reads the first value cause is the lates
  let subscriptionOne = quotes.subscribe{
    print("Subscription 1):", $0)
  }
  quotes.onNext(lackOfFaith)
  quotes.onNext(stayOnTarget)
  
  ///quotes.onError(Quote.neverSaidThat)
  /// this subscription reads the stayOnTarget value cause is the lates
  
  quotes.subscribe{
    print("Subscription 2):",  $0)
  }.disposed(by: disposeBag)
  subscriptionOne.disposed(by: disposeBag)
}

///------------ ReplaySubject --------------

/**
 - Starts empty, with a buffer size
 - Replays buffer to new subscribers
 
  1---2---------3--|>
  1---2---------3--|>
          |1-2--3--|>
 **/

example(of: "ReplaySubject"){
  
  let disposeBag = DisposeBag()
  
  let subject = ReplaySubject<String>.create(bufferSize: 2)
  
  subject.onNext(useTheForce)
  subject.onNext(lackOfFaith)
  
  subject.subscribe{
    print("Subscription 1):", $0)
  }.disposed(by: disposeBag)
  
  subject.onNext(theForceIsStrong)
  /// By here the first subscriber can get all the events event the first two before the subscription
  subject.subscribe{
    print("Subscription 2):", $0)
  }.disposed(by: disposeBag)
  /// By here the second subscriber only gets the two lates events
}

///------------ Variable ------------------

/**
 - Wraps a BehaviourSubject
 - Starts with initial value
 - Replays initial / lates value to new subscribers
 - Guaranteed not to fail
 - Automatically completes
 - Stateful
 **/

example(of: "Variable"){
  
  let disposeBag = DisposeBag()
  
  let variable = Variable(mayTheForceBeWithYou)
  
  print(variable.value)
  
  variable.asObservable().subscribe{
    print("Subscriber 1)", $0)
  }.disposed(by: disposeBag)
  
  variable.value = mayThe4thBeWithYou
  print(variable.value)
}


/*:
 Copyright (c) 2014-2018 Razeware LLC
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
