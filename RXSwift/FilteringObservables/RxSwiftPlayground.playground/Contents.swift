//: Please build the scheme 'RxSwiftPlayground' first
import RxSwift

///     ---------------- FILTERING OBSERVABLES ----------------

/// Ignore elements make the publish subject to ignore all onNext events except a comppleted event.

example(of: "IgnoreElements") {
  let disposeBag = DisposeBag()
  
  let cannedProjects = PublishSubject<String>()
  
  cannedProjects
    .ignoreElements()
    .subscribe{
      print($0)
  }.disposed(by: disposeBag)
  
  cannedProjects.onNext(landOfDroids)
  cannedProjects.onNext(wookieWorld)
  cannedProjects.onNext(detours)
  cannedProjects.onCompleted()
  
  /// Only completed goes through because of ignoreElements.
}

///     --------> Element at


/// Element At function will help you to ignore the events untill the indext you set.
example(of: "elementAt") {
  let disposeBag = DisposeBag()
  
  let quotes = PublishSubject<String>()
  
  quotes.elementAt(2)
    .subscribe(onNext: {
      print($0)
    })
  .disposed(by: disposeBag)
  
  quotes.onNext(mayTheOdds)
  quotes.onNext(liveLongAndProsper)
  /// events 0 and 1 are ignores and only event 2 goes through.
  quotes.onNext(mayTheForce)
  
}

///     --------> FILTER

/// Filter will apply the given predicate to every event and only pass thouse who are true

example(of: "filter") {
  let disposeBag = DisposeBag()
  Observable.from(tomatometerRatings)
    .filter{movie in movie.rating >= 90}
    .subscribe(onNext:{
      print($0)
    })
  .disposed (by: disposeBag)
}

///     --------> SkipWhile

/// This will skip until the condition is false and after that will let pass all the rest of the elements.

example(of: "skipWhile", action: {
  
  let disposeBag = DisposeBag()
  
  Observable.from(tomatometerRatings)
    .skipWhile{movie in movie.rating < 90}
    .subscribe(onNext:{
      print($0)
    }).disposed(by: disposeBag)
  
})

///     --------> Skip Until
/// Skip until get a observable as an  imput and whenever that observable create an event the fir obsevable will stop skiping

example(of: "skipUntil", action: {
   let disposeBag = DisposeBag()
  
  let subject = PublishSubject<String>()
  let trigger = PublishSubject<Void>()
  
  subject.skipUntil(trigger)
    .subscribe(onNext: {
      print($0)
    }).disposed(by: disposeBag)
  
  /// Trigger is passed as an input for subject.skipUntil()
  /// None of this is going to be read until trigger send an event
  
  subject.onNext(episodeV.title)
  subject.onNext(episodeVI.title)
  
  trigger.onNext(())
  
  /// For this time on subjet will get all events cause trigger is not void anymore
  
  subject.onNext(episodeIV.title)
 
})

///     -------->Distinc Until Changed

/// This will not delivered the events repeted in a row

example(of: "distinctUntilChanged", action: {
  let disposeBag = DisposeBag()
  
  Observable<Droid>.of(.R2D2,.C3PO,.C3PO,.R2D2)
  .distinctUntilChanged()
    .subscribe{
      print($0)
  }
  .disposed(by: disposeBag)
})

/// Filtering operator help you to select which data is delivered to subscribers.














/*:
 Copyright (c) 2014-2018 Razeware LLC
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
