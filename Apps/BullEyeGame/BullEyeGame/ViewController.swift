//
//  ViewController.swift
//  BullEyeGame
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-11-18.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Target: UILabel!
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var round: UILabel!
    @IBOutlet weak var score: UILabel!
    var roundNumber : Int = 1 { didSet { round.text = String(roundNumber)}}
    var scoreNumber = 0 {didSet {score.text = String(scoreNumber)}}
    var newTarget = 0 {didSet{Target.text = String(newTarget)}}
    var sliderValue : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderDidMove(slider)
        self.newTarget = Int.random(in: 1...100)
        startOver()
        let imageThumb = #imageLiteral(resourceName: "SliderThumb-Normal")
        slider.setThumbImage(imageThumb, for: .normal)
        let imageHighlightedThumb = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        slider.setThumbImage(imageHighlightedThumb, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft")
        let leftResizeable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(leftResizeable, for: .normal)
        

        let trackRigthtImage = #imageLiteral(resourceName: "SliderTrackRight")
        let rigthResizeable = trackRigthtImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(rigthResizeable, for: .normal)
    }

    @IBAction func informationButton(_ sender: UIButton) {
      self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func PlayAgainButton(_ sender: Any) {
        
        if self.scoreNumber == 0 {
            return
        } else {
            startOver()
            
        }
    }
    @IBAction func showAlert(){
        
        let (result,title) = pointsCalcultor()
        let alertController = UIAlertController(title: title, message: "You hit: \(Int(self.sliderValue)). You got: \(result) points", preferredStyle: .alert)
        
        let alert = UIAlertAction(title: "Next Round", style: .default, handler: nil)
        
        alertController.addAction(alert)
        present(alertController, animated: true, completion: nil)
        
        startNewRound()
    }

    @IBAction func sliderDidMove(_ sender: UISlider) {
        sliderValue = Int(sender.value.rounded())
    }
    
    func startNewRound(){
        let score = pointsCalcultor()
        self.scoreNumber += score.0
        self.newTarget = Int.random(in: 1...100)
        self.roundNumber += 1
        self.slider.setValue(50, animated: true)
        
//        updateLabels(newValue: newValue, newTarget: newTarget, roundValue: round)
    }
//    func updateLabels (newValue: Int, newTarget: Int, roundValue: Int){
//        Target.text = String(newTarget)
//        score.text = String(newValue)
//        self.round.text = String(roundValue)
//        self.view.reloadInputViews()
//
//    }
    
    func pointsCalcultor()-> (Int,String) {
        var difference = sliderValue - newTarget
        let title: String
        if difference < 0 {
            difference *= -1
        }
        switch difference {
        case 0:
            title = "Perfect!"
        case 1...5:
            title = "You almost had it!"
        case 6...10:
            title = "Pretty good!"
        default:
            title = "Not even close!"
        }
        let finalValue = 100 - difference
        
        return (finalValue,title)
    }
    
    func startOver() {
        self.scoreNumber = 0
        self.newTarget = Int.random(in: 1...100)
        self.roundNumber = 1
    }
}

