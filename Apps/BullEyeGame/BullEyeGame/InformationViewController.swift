//
//  InformationViewController.swift
//  BullEyeGame
//
//  Created by Josue Benjamin Cabrera Rincon on 2018-11-19.
//  Copyright © 2018 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit
import WebKit

class InformationViewController: UIViewController {

    @IBOutlet weak var text: UITextView!
    @IBOutlet weak var webText : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 1.
        if let  htmlPath = Bundle.main.path(forResource: "BullsEye", ofType: "html") {
            let url = URL(fileURLWithPath: htmlPath)
            let request = URLRequest(url: url)
            webText.load(request)
        }
        
    }
    
    
}
