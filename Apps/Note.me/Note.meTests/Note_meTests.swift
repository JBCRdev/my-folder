//
//  Note_meTests.swift
//  Note.meTests
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import XCTest
@testable import Note_me

class Note_meTests: XCTestCase {
    
    let folderList = [Folder(name: "folderTest1"),Folder(name: "folderTest2"),Folder(name: "folderTest3"),Folder(name: "folderTest4"),Folder(name: "folderTest5"),Folder(name: "folderTest6"),Folder(name: "folderTest7"),Folder(name: "folderTest8"),Folder(name: "folderTest9"),Folder(name: "folderTest10"),Folder(name: "folderTest11"),Folder(name: "folderTest12")]


    override func setUp() {
        super.setUp()
        
        NoteWriter.folders = folderList
        
    }

    override func tearDown() {
       // sut = nil
        super.tearDown()
    }

    func testFolderDeletion() {
        // 1.Given
        let selectedFoldersToDelete = ["folderTest3":2,"folderTest1":0,"folderTest6":5,"folderTest12":11, "folderTest5":4,"folderTest9":8]
        // 2. When
        
        FolderCreator.folders = folderList
        FolderCreator.deleteFolders(in: selectedFoldersToDelete)
        
        // 3. Then
        XCTAssertEqual(FolderCreator.folders.count, 6, "Deletion method does not delete CORRECTLY")
    }

    func testNoteCreation() {
        // 1. Given
        let noteContent = "TEST CONTENT"
        let noteTitle = "TEST TITLE"
        let folderCount = NoteWriter.folders.count
        // 2. When
          NoteWriter.createNote(withContent:noteContent , title:noteTitle )
        // 3. Then
        XCTAssertEqual(folderCount, folderCount + 1, "Note Creation Failed")
    }
    
    func testNoteDelete() {
        // 1. Given
        let noteContent = "TEST CONTENT"
        let noteTitle = "TEST TITLE"
        //let testNote = Note(content: noteContent, date: Date(), noteTitle: noteTitle)
        NoteWriter.folderToSaveIn = "folderTest3"
        NoteWriter.createNote(withContent: noteContent, title: noteTitle)
        let indexPath = IndexPath(item: 0, section: 2)
        
        // 2. When
        NoteWriter.deleteNote(at: indexPath)
        
        // 3. Then
        
        XCTAssertEqual(NoteWriter.folders[2].noteCount, 0, "Note Celetion Failed")
    }
    
    func testNoteEditing(){
        // 1. Given
        let noteContent = "TEST CONTENT"
        let noteTitle = "TEST TITLE"
        let testNote = Note(content: noteContent, date: Date(), noteTitle: noteTitle)
        NoteWriter.folderToSaveIn = "folderTest3"
        NoteWriter.createNote(withContent: noteContent, title: noteTitle)
        let indexPath = IndexPath(item: 0, section: 2)
        
        let editedContent = "EDITED CONTENT"
        let editedTitle = "EDITED TITLE"
        
        // 2. When
        NoteWriter.update(note: testNote, content: editedContent, title: editedTitle, at: indexPath)
        
        // 3. Then
        
        let editedNote = NoteWriter.folders[indexPath.section].notes[indexPath.item]
        
        XCTAssertEqual(editedNote.content, editedContent, "Note did not edit content properly")
        XCTAssertEqual(editedNote.noteTitle, editedTitle, "Note did not edit title properly")
        
    }

}
