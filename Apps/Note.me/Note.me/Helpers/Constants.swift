//
//  Constants.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import Foundation

enum Idenrifiers: String {
    case folderHeater
    case noteCell
    case createNoteViewController
    case chooFolderVC
}
