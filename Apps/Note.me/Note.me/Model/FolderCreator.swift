//
//  FolderCreator.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import Foundation

struct  FolderCreator {
    
    static var folders = [Folder]()
    
    static func createFolder(name: String )->Folder {
        let folder = Folder(name: name)
        return folder
    }
    
    static func deleteFolders(in diccionary: [String:Int]){
        let sections = diccionary.values.sorted(by:>)
        sections.forEach{ index in
            folders.remove(at: index)
        }
    }
    
}
