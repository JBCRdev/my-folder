//
//  Note.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import Foundation


struct Note {
    
    var content: String
    var date: Date
    var noteTitle: String
    
    func getDateOnFormat()->String {
//        let calendar = Calendar(identifier: .gregorian)
//        let month = calendar.component(.month, from: date)
//        let day = calendar.component(.day, from: date)
//        let year = calendar.component(.year, from: date)
        let formater = DateFormatter()
        formater.dateStyle = .medium
        formater.calendar = Calendar(identifier: .gregorian)
        let stringDate = formater.string(from: date)
        
        return stringDate
    }
}
