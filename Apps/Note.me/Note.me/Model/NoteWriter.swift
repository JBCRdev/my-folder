//
//  NoteWriter.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import Foundation

struct NoteWriter {
    
    static var folders: [Folder] = []
    static var note: Note?
    static var folderToSaveIn = "Notes"
    
    static func createNote(withContent text: String, title: String){
        self.note = Note(content: text, date: Date(), noteTitle: title)
        var index = 0
        var folderToChange: Folder?
        
        for folder in folders {
            if folder.folderName == folderToSaveIn {
                folderToChange = folder
                break
            }
            index += 1
        }
        
        /// change values in the real folder array
        guard folderToChange != nil else {
    
            insertNoteInFolder(at: 0)
            return
        }
        
         insertNoteInFolder(at: index)
    }
    
    static func deleteNote(at indexPath: IndexPath) {
        
        self.folders[indexPath.section].notes.remove(at: indexPath.item)
        
    }
    
    static private func insertNoteInFolder(at index: Int){
        guard let note = self.note else {return}
        folders[index].notes.append(note)
        
    }
    
    static func update(note: Note, content: String, title: String, at indexPath: IndexPath) {
        self.note = note
        self.note?.content = content
        self.note?.noteTitle = title
        replace(note: self.note!, at: indexPath)
        
    }
    
    static private func replace(note: Note, at indexPath: IndexPath){
        
        folders[indexPath.section].notes.remove(at: indexPath.item)
        
        if folders[indexPath.section].notes.isEmpty {
            folders[indexPath.section].notes.append(self.note!)
        } else {
            folders[indexPath.section].notes.insert(self.note!, at: indexPath.item)
            
        }
        
    }
}
