//
//  FolderHeaterCollectionReusableView.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit

protocol FolderHeaterCollectionReusableViewDelegate {
    func folderNameDidTap(in section: Int)
    func didTapFolderWith(name: String, in section: Int, isSelected: Bool)
}

class FolderHeaterCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var folderName: UILabel!
    @IBOutlet weak var notesCount: UILabel!
    
    var section: Int?
    var delegate: FolderHeaterCollectionReusableViewDelegate?
    var folder: Folder? {
        didSet {
            setValues()
        }
    }
    
    var isEditing = false {
        didSet{
            
            if !isEditing {
                setLayOut()
            }
        }
    }
    
    var isSelected: Bool = false {
        didSet {
            self.backgroundColor = isSelected ? UIColor(red: 255, green: 65/255, blue: 55/255, alpha: 0.6) : UIColor.white
            folderName.textColor = isSelected ? UIColor.white : folder?.titleColor
            notesCount.textColor = isSelected ? UIColor.white : folder?.titleColor
            
        }
    }
    
    
    @IBAction func folderNameDidTap(_ sender: UIButton) {
        
        if isEditing {
            isSelected = !isSelected
            guard let section = self.section else{return}
            delegate?.didTapFolderWith(name: self.folder!.folderName, in: section, isSelected: isSelected)
            return
        }
        
        guard let section = self.section else {
            return
        }
        delegate?.folderNameDidTap(in: section )
    }
    
    private func setValues(){
        guard let folder = folder else {
            return
        }
        self.folderName.text = folder.folderName
        self.notesCount.text = String(folder.noteCount)
        setLayOut()
    }
    
     func setLayOut(){
        guard let folder = folder else {
            return
        }
        self.folderName.textColor = folder.titleColor
        self.notesCount.textColor = folder.titleColor
        self.backgroundColor = .white
    }
    
}
