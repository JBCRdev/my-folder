//
//  Folder.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit

struct Folder {
    
    var folderName: String
    var titleColor: UIColor
    var notes: [Note]
    var noteCount: Int {
        return notes.count
    }
    
    init(name: String) {
        self.folderName = name
        self.titleColor = UIColor(red: 255, green: 147/255, blue: 0, alpha: 1)
        self.notes = []
    }
    
    init() {
        self.folderName = "Notes"
        self.titleColor = UIColor(red: 255, green: 147/255, blue: 0, alpha: 1)
        self.notes = [Note(content: "", date: Date(), noteTitle: "TestNote")]
    }
    
   
}

extension Folder: Equatable {
    static func == (lhs: Folder, rhs: Folder) -> Bool {
        return lhs.folderName == rhs.folderName
    }
    
    
}
