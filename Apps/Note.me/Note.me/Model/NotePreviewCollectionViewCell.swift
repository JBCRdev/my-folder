//
//  NotePreviewCollectionViewCell.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit

class NotePreviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var note : Note? {
        didSet {
            setValues()
        }
    }
    
    
    private func setValues() {
        guard let note = self.note else {
            self.setRoundCorners()
            return
        }
        self.noteTitle.text = note.noteTitle
        self.dateLabel.text = note.getDateOnFormat()
        setRoundCorners()
    }
    
    private func setRoundCorners(){
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
    
//    private func setLayout(){
//        self.backgroundColor = UIColor(red: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: <#T##CGFloat#>)
//    }
    
}
