//
//  CreateNoteViewController.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit

protocol CreateNoteViewControllerDelegate: class {
    func newNoteDidSave(folderList: [Folder], in section: Int)
    func noteDidDelete(in section: Int, folderList: [Folder])
}


class CreateNoteViewController: UIViewController {

    @IBOutlet weak var noteTitleField: UITextField!
    @IBOutlet weak var noteContent: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var noteContentHeigh: NSLayoutConstraint!
    private var trashButton: UIBarButtonItem?
    
   
    
    var delegate: CreateNoteViewControllerDelegate?
    var saveButton: UIBarButtonItem?
    var currentNote: Note?
    var noteIndexPath: IndexPath?
    
    let textPlaceHolder = "Note..."
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        noteContent.allowsEditingTextAttributes = true
        scrollView.keyboardDismissMode = .interactive
        navigationController?.isToolbarHidden = false
        setBarButtons()
        setNotifications()
        setDelegates()
        showNote()
        
        
    }
    
    private func setDelegates(){
        noteTitleField.delegate = self
        noteContent.delegate = self
        scrollView.delegate = self
    }
    private func setNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateNoteContentView(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateNoteContentView(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setBarButtons() {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveNoteAction))
        saveButton.isEnabled = false
        self.saveButton = saveButton
        // appent buttonts to right navigation items
        navigationItem.rightBarButtonItems = [saveButton]
        
        let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteNote))
        trashButton.isEnabled = false
        trashButton.tintColor = .red
        self.trashButton = trashButton
        // appent buttonts to toolBar
        
        self.toolbarItems = [trashButton]
        
        
    }
    
    // This functions helps to keep the typing visible from the keyboard adjusting the uitextview content inset
    
    @objc private func updateNoteContentView(notification: Notification) {
        let userInfo = notification.userInfo!
        if let keybordEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as?  NSValue)?.cgRectValue {
            let keybordSize = self.view.convert(keybordEndFrame, to: view.window)
            if notification.name == UIResponder.keyboardWillHideNotification {
                noteContent.contentInset.bottom = .zero
            } else {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keybordSize.height + (noteContentHeigh.constant - view.frame.size.height), right: 0)
                
                
            }
        }
    }
    
    @objc private func saveNoteAction() {
        
        // Only if note is editing, note will be updated
        // If user already created a Note and user is just editing user can't change the folder
        
        if let note = currentNote, let ip = noteIndexPath {
            let content = noteContent.text ?? ""
            let title = noteTitleField.text ?? ""
            NoteWriter.update(note: note, content: content, title: title, at: ip)

            
            delegate?.newNoteDidSave(folderList: NoteWriter.folders, in: ip.section)
            navigationController?.popViewController(animated: true)
            return
        }
        // Otherwise another note from scratch will be created
        // chooseFolder is colled is order to choose the folder a continue with saving
        // once the folder is choosen ChooseFolderViewControllerDelegate function is called (see below)
        
        let chooseFolderVC = storyboard?.instantiateViewController(withIdentifier:
            Idenrifiers.chooFolderVC.rawValue) as! ChooseFolderViewController
        chooseFolderVC.delegate = self
        self.present(chooseFolderVC, animated: true, completion: nil)
        
        
    }
    
    @objc private func deleteNote(){
        NoteWriter.deleteNote(at: noteIndexPath!)
        delegate?.noteDidDelete(in: noteIndexPath!.section, folderList: NoteWriter.folders)
        navigationController?.popViewController(animated: true)
    }

    
    private func showNote(){
        if let note = currentNote {
            noteContent.text = note.content
            noteTitleField.text = note.noteTitle
            noteContent.textColor = .black
            trashButton?.isEnabled = true
            
            
        }
    }
    

}

extension CreateNoteViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton?.isEnabled = true
        currentNote?.date = Date()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        noteContent.becomeFirstResponder()
        return true
    }
}

extension CreateNoteViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let size = CGSize(width: noteContent.frame.size.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        if estimatedSize.height > noteContentHeigh.constant {
            noteContentHeigh.constant = estimatedSize.height
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textPlaceHolder {
            textView.text = ""
        }
        textView.textColor = .black
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        currentNote?.date = Date()
        
        if textView.text == "" {
            saveButton?.isEnabled = false
            
        } else {
            saveButton?.isEnabled = true
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = textPlaceHolder
            textView.textColor = .lightGray
            //saveButton?.isEnabled = false
        }
    }
}

extension CreateNoteViewController:ChooseFolderViewControllerDelegate {
    func folderDidSelected(with section: Int) {
        let content = noteContent.text ?? ""
        let title = noteTitleField.text ?? ""
        NoteWriter.createNote(withContent: content, title: title)
        let newFolderList = NoteWriter.folders
        delegate?.newNoteDidSave(folderList: newFolderList, in: section)
        navigationController?.popViewController(animated: true)
    }
}




