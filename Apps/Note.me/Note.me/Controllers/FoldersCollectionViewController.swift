//
//  FoldersCollectionViewController.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit



class FoldersCollectionViewController: UICollectionViewController {

    

    var folders = [Folder()]
    var displayedFolder : (Folder,Int)? // this variable isused only when deletion is happening
    var viewKind = "UICollectionElementKindSectionHeader"
    lazy var selectedFolders = [String:Int]()
    var trashButton: UIBarButtonItem?
    var addFolderButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = editButtonItem
        setNewNoteButton()
    }

    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return folders.count
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.numberOfItems(for: section)
      
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Idenrifiers.noteCell.rawValue, for: indexPath) as! NotePreviewCollectionViewCell
        
        let currentFolder = folders[indexPath.section]
        cell.note = currentFolder.notes[indexPath.item]
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard !self.isEditing else {
            return
        }
        
        let note = folders[indexPath.section].notes[indexPath.item]
        let viewController = storyboard?.instantiateViewController(withIdentifier: Idenrifiers.createNoteViewController.rawValue) as! CreateNoteViewController
        NoteWriter.folders = self.folders
        viewController.delegate = self
        viewController.currentNote = note
        viewController.noteIndexPath = indexPath
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
  
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Idenrifiers.folderHeater.rawValue, for: indexPath) as! FolderHeaterCollectionReusableView
        
        let currentFolder = folders[indexPath.section]
        reusableView.section = indexPath.section
        reusableView.delegate = self
        reusableView.folder = currentFolder
        self.viewKind = kind
        
        return reusableView
    }
    
    // Editing mode buttons and actions
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        if !folders.isEmpty {
            let indexPaths = self.collectionView.indexPathsForVisibleSupplementaryElements(ofKind: viewKind)

            indexPaths.forEach{ [unowned self] ip in

                let section = self.collectionView.supplementaryView(forElementKind: viewKind, at: ip) as! FolderHeaterCollectionReusableView
                section.isEditing = editing

            }
        }
       
        
        guard editing else {
            setNewNoteButton()
            self.trashButton = nil
            self.addFolderButton = nil
            self.selectedFolders.removeAll()
            return
        }
        
        let deleteButton = deleteFolderButton()
        let addButton = newFolderButton()
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        self.toolbarItems = [deleteButton,flexible,addButton]

    }
    
    
    private func newFolderButton()-> UIBarButtonItem{
        let newFolderButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createNewFolder))
        newFolderButton.tintColor = .blue
        self.addFolderButton = newFolderButton
        return newFolderButton
    }
    
    private func deleteFolderButton()-> UIBarButtonItem{
        let deleteFolderButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteFolder))
        deleteFolderButton.tintColor = .red
        deleteFolderButton.isEnabled = false
        self.trashButton = deleteFolderButton
        return deleteFolderButton
    }
    
    @objc private func deleteFolder(){
        
        FolderCreator.folders = folders
        FolderCreator.deleteFolders(in: selectedFolders)
        self.folders = FolderCreator.folders
        setEditing(false, animated: true)
        collectionView.reloadData()
        
        
    }
    
    private func newFolder(withName folderName: String){
        let new = FolderCreator.createFolder(name: folderName)
        self.folders.append(new)
        setEditing(false, animated: true)
        collectionView.reloadData()
    }
    
    @objc private func createNewFolder(){
        let alert = UIAlertController(title: "New Folder", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: {textField in
            textField.placeholder = "Folder Name"
        })
        
        let createAction = UIAlertAction(title: "Create", style: .default) { [weak self] action in
            guard let folderName = alert.textFields?[0].text else {return}
            self?.newFolder(withName: folderName)
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel)
        
        alert.addAction(createAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setNewNoteButton(){
        let newNoteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "create_new"), style: .done, target: self, action: #selector(createNewNote))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        newNoteButton.tintColor = .darkGray
        navigationController?.toolbar.tintColor = .clear
        self.toolbarItems = [flexible,newNoteButton]
    }
    
    @objc private func createNewNote(){
        let viewController = storyboard?.instantiateViewController(withIdentifier: Idenrifiers.createNoteViewController.rawValue) as! CreateNoteViewController
        NoteWriter.folders = self.folders
        viewController.delegate = self
        navigationController?.pushViewController(viewController, animated: true)
    }

    // This function will say how many items per section must be shown depending on the users tap

    private func numberOfItems(for section: Int)-> Int {

        guard let folder = displayedFolder?.0 else {
            
            return 0
        }

        let itemsForFolder = folders[section]

        if itemsForFolder == folder {
            
            return folder.noteCount
        } else {
            
            return 0
        }
    }
    
    
    private func openEditedNoteFolder(in section: Int){
        self.displayedFolder = (folders[section],section)
        
        collectionView.reloadData()
    }
   

}

extension FoldersCollectionViewController: FolderHeaterCollectionReusableViewDelegate {
    func didTapFolderWith(name: String, in section: Int, isSelected: Bool) {
        if isSelected {
            self.selectedFolders[name] = section
        } else {
            self.selectedFolders.removeValue(forKey: name)
        }
        
        trashButton?.isEnabled = !self.selectedFolders.isEmpty
        addFolderButton?.isEnabled = !self.selectedFolders.isEmpty
    }
    
 
    

    /* This function will help to set the folder shown as displayed folder.
        and to reload the collection view and insert a delete cell from it
    */
    
    func folderNameDidTap(in section: Int) {
        
        
        guard let folderShown = displayedFolder else {
            /// - condition One: shows items from sections when no folder is selected
            
            self.displayedFolder = (folders[section],section)
            
            UIView.animate(withDuration: 0.6, animations: { [weak self] in
                self?.collectionView.reloadSections(IndexSet(arrayLiteral: section))
            })
           
            return
        }
        
        /// - condition Two: shows and disapper items from sections when one folder is shown and user taps the same section to open or close the folder.
        
        if folderShown.0 == folders[section] {
            // this prevents the only folder to be closed
            if folders.count == 1 {
                
                collectionView.reloadData()
                
            }
            displayedFolder = nil
            UIView.animate(withDuration: 0.6, animations: { [weak self] in
                self?.collectionView.reloadSections(IndexSet(arrayLiteral: section))
            })
            
        
        } else {
            
            /// - contition Three: shows and disapper items from sections when one folder is shown and user taps another  section to open the folder and close the previus folder shown.
            let folderSectionToDissapear = folderShown.1
            // 1) close all folders updating the opened folder section
            displayedFolder = nil
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.collectionView.reloadSections(IndexSet(arrayLiteral: folderSectionToDissapear))
            })
            
            // 2) set the displayedFolder base on the taped section and open the new selected section
            
            displayedFolder = (folders[section],section)
            UIView.animate(withDuration: 0.6, animations: { [weak self] in
                self?.collectionView.reloadSections(IndexSet(arrayLiteral: section))
            })
            
        }
    }
    
    
}

extension FoldersCollectionViewController: CreateNoteViewControllerDelegate {
    func newNoteDidSave(folderList: [Folder], in section: Int) {
        self.folders = folderList
        displayedFolder = nil
        openEditedNoteFolder(in: section)

    }
    
    func noteDidDelete(in section: Int, folderList: [Folder]) {
        self.folders = folderList
        openEditedNoteFolder(in: section)
    }
    
}

extension FoldersCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 360, height: 60)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return CGFloat(10)
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
    }
}
