//
//  ChooseFolderViewController.swift
//  Note.me
//
//  Created by Josue Benjamin Cabrera Rincon on 2019-06-28.
//  Copyright © 2019 Josue Benjamin Cabrera Rincon. All rights reserved.
//

import UIKit

protocol ChooseFolderViewControllerDelegate: class {
    func folderDidSelected(with section: Int)
}


class ChooseFolderViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var folderDisplayLabel: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var menuView: UIView!
    lazy var folderPicker = UIPickerView()
    var sectionPicked : Int?
    var delegate:ChooseFolderViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setLayouts()
        setFolderPicker()
    }

    @IBAction func doneAction(_ sender: UIButton) {
        NoteWriter.folderToSaveIn = folderDisplayLabel.text ?? "Notes"
        self.dismiss(animated: true, completion: nil)
        delegate?.folderDidSelected(with: sectionPicked ?? 0)
        
        
    }
    
    private func setLayouts(){
        menuView.layer.cornerRadius = 8
        menuView.layer.masksToBounds = true
        doneButton.layer.cornerRadius = 8
        doneButton.layer.masksToBounds = true
        if !NoteWriter.folders.isEmpty{
            folderDisplayLabel.text = NoteWriter.folders[0].folderName
        } else {folderDisplayLabel.text = "No folders"}
    }
    
    private func setFolderPicker(){
        folderPicker.dataSource = self
        folderPicker.delegate = self
        folderDisplayLabel.inputView = folderPicker
        toolBarForPicker()
        
    }
    
    private func toolBarForPicker(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(folderDidSelect))
        toolbar.setItems([doneButton], animated: true)
        toolbar.isUserInteractionEnabled = true
       folderDisplayLabel.inputAccessoryView = toolbar
    }
    
    @objc private func folderDidSelect(){
        folderDisplayLabel.resignFirstResponder()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChooseFolderViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return NoteWriter.folders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return NoteWriter.folders[row].folderName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        folderDisplayLabel.text = NoteWriter.folders[row].folderName
        sectionPicked = row
    }
    
}
