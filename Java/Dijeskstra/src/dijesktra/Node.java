package dijesktra;

import java.util.ArrayList;

public class Node
{
    //PROPERTIES
    private String frontLabel;
    private int value;
    private boolean visitedFlag;
    private String name;
    private ArrayList <Connection> cn;

    //CONSTRUCTOR

    public Node(String frontLabel, int value, boolean visitedFlag, String name) {
        this.frontLabel = frontLabel;
        this.value = value;
        this.visitedFlag = visitedFlag;
        this.name = name;
        this.cn = new ArrayList <Connection>();
    }


    //LANGUAGES


    public String getFrontLabel() {
        return frontLabel;
    }

    public int getValue() {
        return value;
    }

    public boolean isVisitedFlag() {
        return visitedFlag;
    }

    public String getName() {
        return name;
    }

    public ArrayList <Connection> getCn() {
        return cn;
    }

    public void setFrontLabel(String frontLabel) {
        this.frontLabel = frontLabel;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setVisitedFlag(boolean visitedFlag) {
        this.visitedFlag = visitedFlag;
    }
}
