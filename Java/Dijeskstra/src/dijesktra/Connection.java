package dijesktra;

public class Connection {

    //PROPERTIES
   private Node source;
   private Node dest;
   private int value;
    //CONSTRUCTOR

    public Connection(Node source, Node dest, int value) {
        this.source = source;
        this.dest = dest;
        this.value = value;
    }

    //LANGUAGE


    public Node getSource() {
        return source;
    }

    public Node getDest() {
        return dest;
    }

    public int getValue() {
        return value;
    }
}
