package dijesktra;

import java.util.ArrayList;

public class ShortestPath {

    //PROPERTIES
    private Node dest;
    private  Node source;
    private ArrayList <String> path;
    private int totalValue;

    //CONSTRUCTOR

    public ShortestPath(Node dest, Node source, int totalValue) {
        this.dest = dest;
        this.source = source;
        this.totalValue = totalValue;
    }

    //LANGUAGE


    public Node getDest() {
        return dest;
    }

    public Node getSource() {
        return source;
    }

    public ArrayList<String> getPath() {
        return path;
    }

    public int getTotalValue() {
        return totalValue;
    }

    public void setPath(ArrayList <String> path) {
        this.path = path;
    }

    public void setTotalValue(int totalValue) {
        this.totalValue = totalValue;
    }
}

