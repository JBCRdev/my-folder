package dijesktra;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.xpath.internal.functions.FuncFalse;
import org.omg.PortableInterceptor.DISCARDING;

import javax.print.DocFlavor;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {


        ArrayList<Node> nodeList = new ArrayList<>();

        ArrayList<Connection> conneList = new ArrayList<>();

        Node nodeA = new Node("X", 32000, false, "A");
        Node nodeB = new Node("X", 32000, false, "B");
        Node nodeC = new Node("X", 32000, false, "C");
        Node nodeD = new Node("X", 32000, false, "D");
        Node nodeE = new Node("X", 32000, false, "E");

        nodeList.add(nodeA);
        nodeList.add(nodeB);
        nodeList.add(nodeC);
        nodeList.add(nodeD);
        nodeList.add(nodeE);

        Connection AE = new Connection(nodeA, nodeE, 2);
        Connection BA = new Connection(nodeB, nodeA, 7);
        Connection BC = new Connection(nodeB, nodeC, 9);
        Connection CD = new Connection(nodeC, nodeD, 3);
        Connection DB = new Connection(nodeD, nodeB, 15);
        Connection DE = new Connection(nodeD, nodeA, 8);
        Connection EB = new Connection(nodeE, nodeB, 7);
        Connection EC = new Connection(nodeE, nodeC, 10);

        conneList.add(AE);
        conneList.add(BA);
        conneList.add(BC);
        conneList.add(CD);
        conneList.add(DB);
        conneList.add(DE);
        conneList.add(EB);
        conneList.add(EC);

        Graph graph = new Graph(nodeList, conneList);

        ShortestPath sh=new ShortestPath(nodeE,nodeD,0);

        Dijisktra diji = new Dijisktra(sh.getSource(),sh.getDest(),sh, graph);

        ArrayList <String> pathNodes = new ArrayList<String>();
        sh.setPath(pathNodes);
        graph.connectionGiver();

        Node source = diji.getSource();
        source.setValue(0);
        diji.update(source);

       for (int i = 0; i < graph.getNodes().size()-1; i++) {
           Node nextNode = diji.pickNextNode();
           diji.update(nextNode);
       }
       diji.Sequence();
       diji.print();
    }
}
