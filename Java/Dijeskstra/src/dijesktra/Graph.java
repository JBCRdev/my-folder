package dijesktra;

import java.util.ArrayList;

public class Graph {

    //PROPERTIES
    private ArrayList <Node> nodes;
    private ArrayList <Connection> edges;
    //CONSTRUCTOR


    public Graph(ArrayList<Node> nodes, ArrayList<Connection> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }
    //LANGUAGE
    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void connectionGiver(){
        for(int i=0; i<this.nodes.size(); i++) {
            for(int j=0; j<this.edges.size(); j++) {

                String e=this.edges.get(j).getSource().getName();
                String n=this.nodes.get(i).getName();
                if(n==e)
                {
                    this.nodes.get(i).getCn().add(this.edges.get(j));
                }
            }
        }

    }
}
