package dijesktra;

import org.omg.PortableServer.POAPackage.NoServant;

import java.util.ArrayList;

public class Dijisktra {

    //PROPERTIES
    private Node source;
    private Node dest;
    private ShortestPath sh;
    private Graph graph;

    //CONSTRUCTOR

    public Dijisktra(Node source, Node dest, ShortestPath sh, Graph graph) {
        this.source = source;
        this.dest = dest;
        this.sh = sh;
        this.graph = graph;
    }

    //LANGUAGE


    public Node getSource() {
        return source;
    }

    public Node pickNextNode()
    {
        int min=1000;

        for (int i=0;i<this.graph.getNodes().size();i++){
            Node x;
            x=this.graph.getNodes().get(i);
            if(!(x.isVisitedFlag()) && min>x.getValue())
            {
                min=x.getValue();
            }
        }
        Node r=null;
        for (int i = 0; i<this.graph.getNodes().size(); i++){
            Node x;
            x=this.graph.getNodes().get(i);
            if (x.getValue()==min)
            {
                r = x;
            }
        }
        return r;
    }
    public void update(Node x){
            for (int c = 0;c < x.getCn().size(); c++){
                int y;
                y = x.getCn().get(c).getDest().getValue();
                int z= x.getCn().get(c).getValue();
                int u=x.getValue()+z;
                if (u < y) {
                    x.getCn().get(c).getDest().setValue(u);
                    x.getCn().get(c).getDest().setFrontLabel(x.getName());
                }
            }
        x.setVisitedFlag(true);
    }
    public void Sequence (){
        sh.getPath().add(dest.getName());
        Node z=dest;
        while (!(z.getFrontLabel()=="X")){
            for (int i = 0; i<this.graph.getNodes().size(); i++){
                if (z.getFrontLabel().equals(this.graph.getNodes().get(i).getName())){
                    z=this.graph.getNodes().get(i);
                    String w=z.getName();
                    sh.getPath().add("<-"+w);
                }
            }
        }
        sh.setTotalValue(dest.getValue());
    }

    public void print (){
        int t=sh.getTotalValue();
        System.out.print(t+"|");
        for (int a=0; a<sh.getPath().size();a++){
            String w =sh.getPath().get(a);
            System.out.print(w);
        }
    }
}
