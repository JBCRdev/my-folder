package Problem2;

import java.util.ArrayList;
import java.util.Arrays;

public class ApplicationDriver {
    public static void main(String [] arg){
        //AUTOBOTS
        Transformer hound=new Autobots("Optimous Prime",Transformer.Team.Autobots,9,10,9,8,10,90,10,9);
        Transformer grimlock=new Autobots("Grimlock",Transformer.Team.Autobots,9,6,7,8,4,5,8,10);
        Transformer bumblebee=new Autobots("Bumblebee",Transformer.Team.Autobots,9,0,7,9,8,10,6,8);
        ArrayList<Transformer> autobotsTeam=new ArrayList<>(Arrays.asList(hound,grimlock,bumblebee));
        //DECEPTICONS
        Transformer starscream=new Decepticons("Starscream",Transformer.Team.Decepticons,8,7,9,6,7,9,4,5);
        Transformer barricade=new Decepticons("Barricade",Transformer.Team.Decepticons,5,4,6,7,2,3,4,5);
        ArrayList<Transformer> decepticons=new ArrayList<>(Arrays.asList(starscream,barricade));

        //Battle
        BattleField battle=new BattleField(autobotsTeam,decepticons);
        battle.referee();




    }
}
