package Problem2;

import com.sun.java.swing.action.FinishAction;

import javax.print.attribute.standard.Finishings;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;


public class BattleField {

    private ArrayList<Transformer> autobotsTeam;
    private ArrayList<Transformer> decepticonsTeam;

    public BattleField(ArrayList<Transformer> autobotsTeam, ArrayList<Transformer> decepticonsTeam) {
        this.autobotsTeam = autobotsTeam;
        this.decepticonsTeam = decepticonsTeam;
    }

    public ArrayList<Transformer> getAutobotsTeam() {
        return autobotsTeam;
    }

    public ArrayList<Transformer> getDecepticonsTeam() {
        return decepticonsTeam;
    }
    class Score{
        private int numOfBattles;
        private ArrayList<Transformer> autobotswin;
        private ArrayList<Transformer> decepticonswin;
        private ArrayList<Transformer> survivors;
        private String winner;
        private String loser;
        public Score() {
            this.numOfBattles =0;
            this.autobotswin =new ArrayList<>();
            this.decepticonswin =new ArrayList<>();
            this.survivors =new ArrayList<>();
            this.winner="...";
            this.loser="...";
        }


        public String getWinner() {
            return winner;
        }

        public void setWinner(String winner) {
            this.winner = winner;
        }

        public String getLoser() {
            return loser;
        }

        public void setLoser(String loser) {
            this.loser = loser;
        }

        public int getNumOfBattles() {
            return numOfBattles;
        }

        public void setNumOfBattles(int numOfBattles) {
            this.numOfBattles = numOfBattles;
        }

        public ArrayList<Transformer> getAutobotswin() {
            return autobotswin;
        }

        public ArrayList<Transformer> getDecepticonswin() {
            return decepticonswin;
        }

        public ArrayList<Transformer> getSurvivors() {
            return survivors;
        }

        public void setSurvivors(ArrayList<Transformer> survivors) {
            this.survivors = survivors;
        }
    }
    class Functional{
    public Transformer difference(Transformer a,Transformer b){
        int ratingA=a.getStrength()+a.getIntelligence()+a.getSpeed()+a.getFirepower();
        int ratingB=b.getStrength()+b.getIntelligence()+b.getSpeed()+b.getFirepower();
        int result=ratingA-ratingB;
        if(result>=1){return a;}
        else if(result<=-1){return b;}
        else {return new Transformer("tie",Transformer.Team.Decepticons,0,0,0,0,0,0,0,0);
    }
    }
}
    static class Functional2{
        static int analyser(Transformer winner, Score z){
            String n=winner.getName();
            if(n.matches("EveryBody Dead")){Printer.printdead(n); return 1;}
            else if (winner instanceof Autobots){z.autobotswin.add(winner); return 0;}
            else if (winner instanceof Decepticons){z.decepticonswin.add(winner); return 0;}
            return 0;
        }
    }
    static class Printer{
        public static void printdead(String x){System.out.println("\n"+x+" -> Primes battle");}
        public static void mainPrinter(Score s,ArrayList<Transformer> a, ArrayList<Transformer> d) {
            System.out.println("**** THE WINNERS ARE: " + s.getWinner() + " *****");

            if(s.getWinner().matches("Autobots")) {
                int c = 1;
                for (Transformer z : a) {
                    System.out.println(c + "- " + z.getName());
                    c = c + 1;
                }
            }
            else {
                int c = 1;
                for (Transformer z : d) {
                    System.out.println(c + "- " + z.getName());
                    c = c + 1;
                }
            }

            System.out.println("\n-Number of battles: " + "" + s.getNumOfBattles());

            if (s.getSurvivors().size() > 0) {
                for (int i = 0; i < s.getSurvivors().size(); i++) {
                    Transformer x = s.getSurvivors().get(i);
                    System.out.println("\n-The Survivor(s) from Team (" + x.getTeam() + ") is(are): \n" + (i + 1) + "-" + x.getName());
                }
            }
            else {System.out.println("\nAll Transformers have fought.");}
        }
    }
    public int referee(){
        Score score=new Score();
        //
        BiPredicate<Integer,Integer> pre=(a,d)->a>d;
        ArrayList<Transformer> minor=counter(pre);
        int battles=minor.size();
        score.setNumOfBattles(battles);
        ArrayList<Transformer> mayor;
        if (minor.get(0)instanceof Autobots){mayor=decepticonsTeam;}
        else {mayor=autobotsTeam;}
        int j=0;
        for (int i=0;i<minor.size();i++){
            Transformer winner=battle(minor.get(i),mayor.get(j));
            BiFunction<Transformer,Score,Integer> consumer=Functional2::analyser;
            int result=consumer.apply(winner,score);
            if (result==1){return 0;}
            System.out.println("Battle("+(i+1)+") Winner: "+winner.getName()+"\n");
            j=j+1;
        }
        //
        if(score.getAutobotswin().size()>score.getDecepticonswin().size()){score.setWinner("Autobots"); score.setLoser("Decepticons");}
        else{score.setWinner("Decepticons"); score.setLoser("Autobots");}
        int survivors=mayor.size()-minor.size();
        int v=1;
        for (int i=survivors;i>0;i--){
            int index=mayor.size()-v;
            score.getSurvivors().add(mayor.get(index));
            v=v+1;
        }
        Printer.mainPrinter(score,autobotsTeam,decepticonsTeam);
        return 0;
    }
    private ArrayList<Transformer> counter(BiPredicate<Integer, Integer> x ){
        int a=autobotsTeam.size();
        int d=decepticonsTeam.size();
        boolean minor=x.test(a,d);
        if (minor){return  decepticonsTeam;}
        else return autobotsTeam;
    }
    private Transformer battle(Transformer a, Transformer b){
        Transformer auto;
        Transformer desep;
        String namea;
        String nameb;
        if (a.getTeam().equals(Transformer.Team.Autobots)){namea=a.getName();nameb=b.getName();auto=a;desep=b;}
        else {nameb=a.getName();namea=b.getName(); auto=b; desep=a;}
        System.out.println("\t\t-Battle-\n\n"+namea+" vs "+nameb);
        if (namea.matches("Optimous Prime")&&nameb.matches("Predaking")){
            Transformer n=new Transformer("EveryBody Dead",Transformer.Team.Autobots,0,0,0,0,0,0,0,0);return n;}
            else if(namea.matches("Optimous Prime")){return auto;}
            else if(nameb.matches("Predaking")){return desep;}
            else{ BiFunction<Integer,Integer,Integer> bif=(x,z)->x-z; return fight(a,b,bif);}
    }
    private Transformer fight(Transformer a, Transformer b, BiFunction<Integer,Integer,Integer> x){
            int courage=x.apply(a.getCourage(),b.getCourage());
            int strength=x.apply(a.getStrength(),b.getStrength());
            int skill=x.apply(a.getSkill(),b.getSkill());
            if(courage>=4&strength>=4){return a;}
            else if(courage<=-4&strength>=-4){return b;}
            else if(skill>=3){return a;}
            else if(skill<=-3){return b;}
            else {BiFunction<Transformer,Transformer,Transformer> f=new Functional()::difference;
            Transformer winner=f.apply(a,b); return winner;
            }


        }
}
