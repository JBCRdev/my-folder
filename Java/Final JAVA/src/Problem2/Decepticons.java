package Problem2;

public class Decepticons extends Transformer {
    public Decepticons(String name, Team team, int strength, int intelligence, int speed, int endurance, int rank, int courage, int firepower, int skill) {
        super(name, team, strength, intelligence, speed, endurance, rank, courage, firepower, skill);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public Team getTeam() {
        return super.getTeam();
    }

    @Override
    public int getStrength() {
        return super.getStrength();
    }

    @Override
    public int getIntelligence() {
        return super.getIntelligence();
    }

    @Override
    public int getSpeed() {
        return super.getSpeed();
    }

    @Override
    public int getEndurance() {
        return super.getEndurance();
    }

    @Override
    public int getRank() {
        return super.getRank();
    }

    @Override
    public int getCourage() {
        return super.getCourage();
    }

    @Override
    public int getFirepower() {
        return super.getFirepower();
    }

    @Override
    public int getSkill() {
        return super.getSkill();
    }
}
