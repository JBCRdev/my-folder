package Problem1;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.BiPredicate;
import java.util.function.IntPredicate;


public class Simplifier {
    private ArrayList<Integer> simplist;
    private ArrayList<Integer> listCopy;

    public Simplifier(ArrayList<Integer> simplist, ArrayList<Integer> listCopy) {
        this.simplist = simplist;
        this.listCopy = listCopy;
    }

    public ArrayList<Integer> getSimplist() {
        return simplist;
    }

    public void setListCopy(ArrayList<Integer> listCopy) {
        this.listCopy = listCopy;
    }

    class Container{
        private int courrent;
        private Boolean equal;
        private int score;

        public Container(int courrent, Boolean equal, int score) {
            this.courrent = courrent;
            this.equal = equal;
            this.score = score;
        }

        public int getCourrent() {
            return courrent;
        }

        public void setCourrent(int courrent) {
            this.courrent = courrent;
        }

        public Boolean getEqual() {
            return equal;
        }

        public void setEqual(Boolean equal) {
            this.equal = equal;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }
    }

    static class Comparator{

        public static boolean compare(int x,int y){
            if (x==y){return true;}
            else return false;

        }
    }


    public ArrayList<Integer> work(){
        BiPredicate<Integer,Integer> bi=Comparator::compare;
        int count=0;
        Container cont=new Container(0,false,0);
        for (int i=0;i<simplist.size();i++){
            cont.setCourrent(i);
            boolean max;
            if (i!=simplist.size()-1){
                int r=i+1;
                max=bi.test(simplist.get(i),simplist.get(r));
                if (max){cont.setEqual(true); count=count+1; cont.setScore(count);}
                else if(cont.getScore()==0){cont.setEqual(false);cont.setScore(0);cont.setCourrent(0);count=0;}
                else{remover(i,cont.getScore()); cont.setEqual(false);cont.setScore(0);cont.setCourrent(0); count=0;}

            }
        }
        IntPredicate in=(x)->x!=0;
        splitter(in);
        System.out.println(listCopy);
        return listCopy;
    }

    public void remover(int x, int score){

        for (int i=0;i<score;i++){
            int remove=x-i;
            listCopy.set(remove,0);

        }
    }
    public void splitter(IntPredicate c){
        Iterator<Integer> it=listCopy.iterator();
        ArrayList<Integer> splitted=new ArrayList<>();
        while (it.hasNext()){
            int z=it.next();
            if(c.test(z)){
                splitted.add(z);
            }
            setListCopy(splitted);
        }
    }
}


