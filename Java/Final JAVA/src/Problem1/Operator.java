package Problem1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class Operator {
    private ArrayList<Integer> stretchList;
    private ArrayList<Integer> valleyList;
    private ArrayList<Integer> peakList;
    private ArrayList<Integer> notBuild;

    public Operator(ArrayList<Integer> stretchList) {
        this.stretchList = stretchList;
        this.valleyList=new ArrayList<>();
        this.peakList=new ArrayList<>();
        this.notBuild=new ArrayList<>();
    }

    public ArrayList<Integer> getValleyList() {
        return valleyList;
    }

    public void setValleyList(ArrayList<Integer> valleyList) {
        this.valleyList = valleyList;
    }

    public ArrayList<Integer> getPeakList() {
        return peakList;
    }

    public void setPeakList(ArrayList<Integer> peakList) {
        this.peakList = peakList;
    }

    public class Mid {

        private boolean previous;
        private boolean following;

        public Mid(boolean previous, boolean following) {
            this.previous = previous;
            this.following = following;
        }

        public boolean isPrevious() {
            return previous;
        }

        public void setPrevious(boolean previous) {
            this.previous = previous;
        }

        public boolean isFollowing() {
            return following;
        }

        public void setFollowing(boolean following) {
            this.following = following;
        }
    }

     class Printer{
        public  void print(){
            System.out.println("You can build: "+ valleyList.size()+" castles in valleys");
            System.out.println("You can build: "+ peakList.size()+" castles in peaks");
            System.out.println("You can not build castles in index: "+notBuild);
        }
    }


    public void usher(BiPredicate<Integer,Integer> x){
        firstSetter();
        BiFunction<Boolean,Boolean,Integer> func=(a,b)->{
            if(a&b){return 1;}
            else if(!a&!b){return -1;}
            else return 0;
        };
        Iterator<Integer> str=stretchList.iterator();

       for(int i=1;i<stretchList.size()-1;i++){
           Mid mid=new Mid(false,false);
           boolean previous=x.test(stretchList.get(i),stretchList.get(i-1));
           if(previous){mid.setPrevious(true);}
           boolean following=x.test(stretchList.get(i),stretchList.get(i+1));
           if (following){mid.setFollowing(true);}
           int answer=judge(mid,func);
           int index=stretchList.get(i);
           if (answer==0){notBuild.add(i);}
           else if(answer==1){peakList.add(index);}
           else {valleyList.add(index);}
        }
        new Printer().print();
    }
    public int judge(Mid m,BiFunction<Boolean,Boolean,Integer> b){
        int veredict=b.apply(m.isPrevious(),m.isFollowing());
        return veredict;
    }
    public  void firstSetter(){
        int first=stretchList.get(0);
        int index=stretchList.size();
        int last=stretchList.get(index-1);
        valleyList.add(first);
        if(last>stretchList.get(index-2)){peakList.add(last);}
        else {valleyList.add(last);}
    }




}
