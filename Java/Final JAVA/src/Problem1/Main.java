package Problem1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.BiPredicate;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> theList = new ArrayList<>(Arrays.asList(1,2,4,3,5));
        Simplifier simp = new Simplifier(theList,theList);
        BiPredicate<Integer,Integer> bi=(x,y)->x>y;
        Operator op=new Operator(simp.work());
        op.usher(bi);
    }


}