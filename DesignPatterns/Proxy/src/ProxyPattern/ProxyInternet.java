package ProxyPattern;

import java.util.ArrayList;

public class ProxyInternet implements Internet {

    Internet internet;
    private static ArrayList<String> bannedSites;

    static {
        bannedSites = new ArrayList<>();
        bannedSites.add("abc.com");
        bannedSites.add("def.com");
        bannedSites.add("ijk.com");
        bannedSites.add("lnm.com");
    }

    @Override
    public void connectTo(String serverHost) throws Exception {
        tryInternet();

        if (bannedSites.contains(serverHost.toLowerCase())) {
            throw new Exception("Can't access to: "+serverHost);
        }

        this.internet.connectTo(serverHost);


    }

    private void tryInternet() {
        if (this.internet == null ){
            this.internet = new RealInternet();
        }
    }
}
