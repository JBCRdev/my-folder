package ProxyPattern;

public class Main {

    public static void main(String[] args) {

        ProxyInternet proxy = new ProxyInternet();

        try {
            proxy.connectTo("me.com");
            proxy.connectTo("abc.com");
        } catch (Exception e) {

            System.out.println(e.getMessage());
        }
    }
}
