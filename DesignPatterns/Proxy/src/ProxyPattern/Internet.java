package ProxyPattern;

public interface Internet {

    void connectTo(String serverHost) throws Exception;
}
