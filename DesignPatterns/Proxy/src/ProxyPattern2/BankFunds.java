package ProxyPattern2;

public class BankFunds implements Transaction {

    private float funds = 150;

    @Override
    public void fundWithdrawal(float amount) throws Exception {
        System.out.println("Withdrawal of $"+amount+"0 Successful");
        this.funds = funds-amount;
    }

    public float getFunds() {
        return funds;
    }
}
