package ProxyPattern2;

public interface Transaction {

    void fundWithdrawal(float amount) throws Exception;
}
