package ProxyPattern2;

public class CheckProxy implements Transaction {

    BankFunds banck;



    @Override
    public void fundWithdrawal(float amount) throws Exception {
        tryBankAccount();

        if (banck.getFunds() < amount) {
            throw new Exception("Impossible to withdrawal. Insufficient funds.");
        }

        banck.fundWithdrawal(amount);
    }

    void tryBankAccount(){
        if (this.banck == null) {
            this.banck = new BankFunds();
        }
    }
}
