package ProxyPattern2;

public class BanckTransaction {

    static public void main(String[] args){
        Transaction check = new CheckProxy();

        try {
            check.fundWithdrawal(100);
            check.fundWithdrawal(50);
            check.fundWithdrawal(250);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
