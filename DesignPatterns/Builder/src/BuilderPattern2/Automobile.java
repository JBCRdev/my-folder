package BuilderPattern2;

public interface Automobile {
    String typeOfEngine();
    int doors();
    float totalCoast();
    int yearsOfWarranty();
}
