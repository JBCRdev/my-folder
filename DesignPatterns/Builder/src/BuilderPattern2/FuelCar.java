package BuilderPattern2;

public class FuelCar extends Car {
    @Override
    public String typeOfEngine() {
        Engine engine = new FuelEngine();

        return engine.engine();
    }

    @Override
    public float totalCoast() {
        return 25_000f;
    }

    @Override
    public int yearsOfWarranty() {
        return 4;
    }
}
