package BuilderPattern2;

public class FuelEngine extends Engine {

    @Override
    public String engine() {
        return "Fuel Engine";
    }
}
