package BuilderPattern2;

public class HybridCar extends Car {
    @Override
    public String typeOfEngine() {
        Engine engine = new HybridEngine();

        return engine.engine();
    }

    @Override
    public float totalCoast() {
        return 34_000f;
    }

    @Override
    public int yearsOfWarranty() {
        return 7;
    }
}
