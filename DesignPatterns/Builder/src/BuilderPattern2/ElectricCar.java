package BuilderPattern2;

public class ElectricCar extends Car {
    @Override
    public String typeOfEngine() {
        Engine engine = new ElectricEngine();
        return engine.engine();
    }

    @Override
    public float totalCoast() {
        return 50_000f;
    }

    @Override
    public int yearsOfWarranty() {
        return 10;
    }
}
