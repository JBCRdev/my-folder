package BuilderPattern2;

public class HybridEngine extends Engine {
    @Override
    public String engine() {
        return "Hybrid Engine";
    }
}
