package BuilderPattern2;

public interface EngineTechnology {
    String engine();
}
