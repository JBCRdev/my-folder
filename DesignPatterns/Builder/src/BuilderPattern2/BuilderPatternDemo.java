package BuilderPattern2;

public class BuilderPatternDemo {
    public static void main(String[] args) {

        CardBuilder cB = new CardBuilder();

        CarOrder fuelCar = cB.buildFuelCar();
        System.out.println("** Fuel Car **");
        fuelCar.showFeatures();
        fuelCar.showWaaranty();

        CarOrder hybridCar = cB.buildHybritCar();
        System.out.println("** Hybrid Car **");
        hybridCar.showFeatures();
        hybridCar.showWaaranty();

        CarOrder electricCar = cB.buildElectricCar();
        System.out.println("** Electric Car **");
        electricCar.showFeatures();
        electricCar.showWaaranty();

    }
}
