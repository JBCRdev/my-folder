package BuilderPattern2;

public class ElectricEngine extends Engine {
    @Override
    public String engine() {
        return "Electric Engine";
    }
}
