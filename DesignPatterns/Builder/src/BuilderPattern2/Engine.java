package BuilderPattern2;

public abstract class Engine implements EngineTechnology {

    @Override
    public abstract String engine();
}
