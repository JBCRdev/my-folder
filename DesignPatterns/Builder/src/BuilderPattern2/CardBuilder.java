package BuilderPattern2;

public class CardBuilder {

    public CarOrder buildHybritCar() {
        CarOrder carOrder = new CarOrder();
        Car carBuilt = carOrder.builCar(new HybridCar());
        return carOrder;
    }

    public CarOrder buildElectricCar() {
        CarOrder carOrder = new CarOrder();
        Car carBuilt = carOrder.builCar(new ElectricCar());
        return carOrder;
    }

    public CarOrder buildFuelCar() {
        CarOrder carOrder = new CarOrder();
        Car carBuilt = carOrder.builCar(new FuelCar());
        return carOrder;
    }
}
