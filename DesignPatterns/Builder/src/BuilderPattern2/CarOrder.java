package BuilderPattern2;

public class CarOrder {

    private Car carToBuild;

    public Car builCar(Car car){
         this.carToBuild = car;

         return this.carToBuild;
    }

    public void showFeatures(){
        System.out.println("Engine: " + carToBuild.typeOfEngine());
        System.out.println("Number of Doors: " + carToBuild.doors());
        System.out.println("Price: $" + carToBuild.totalCoast() + "CAD");
    }

    public void showWaaranty(){
        System.out.println("Warranty: " + carToBuild.yearsOfWarranty() + " Years \n");
    }
}
