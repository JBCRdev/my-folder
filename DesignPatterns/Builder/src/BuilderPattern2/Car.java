package BuilderPattern2;

public abstract class Car implements Automobile {


    @Override
    public int doors() {
        return 4;
    }

    @Override
    public abstract float totalCoast();

    @Override
    public abstract int yearsOfWarranty();
}
