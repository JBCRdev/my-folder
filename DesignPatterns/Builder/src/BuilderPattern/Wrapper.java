package BuilderPattern;

public class Wrapper implements  Package {
    @Override
    public String pack() {
        return "wrapper";
    }
}
