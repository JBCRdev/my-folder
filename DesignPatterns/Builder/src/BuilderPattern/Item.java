package BuilderPattern;

public interface Item {
     String name();
     Package packing();
     float price();

}
