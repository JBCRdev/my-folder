package BuilderPattern;

import java.util.ArrayList;

public class Meal {

    ArrayList<Item> itemList = new ArrayList<>();

    public void addItem (Item item){

        itemList.add(item);
    }

    public float getPrice() {
        float totalPrice = 0;

        for (Item item : itemList) {

           totalPrice += item.price();
        }
        return  totalPrice;
    }

    public void showItems(){

        for (Item item : itemList) {

            System.out.print("Item: " + item.name());
            System.out.print(", Packing: " + item.packing().pack());
            System.out.println(", price: " + item.price());
        }
    }

}
