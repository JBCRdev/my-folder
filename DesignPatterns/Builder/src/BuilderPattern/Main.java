package BuilderPattern;

public class Main {

    public static void main(String[] args) {

        MealBuilder mealBuilder = new MealBuilder();

        Meal vegBurger = mealBuilder.prepareVegBurger();

        System.out.println("VegBurger: ");
        vegBurger.showItems();
        System.out.println("Total Cost: " + vegBurger.getPrice()+"\n");


        Meal  chickenBurger = mealBuilder.prepareChickenBurger();
        System.out.println("Chicken Burger: ");
        chickenBurger.showItems();
        System.out.println("Total cost: " + chickenBurger.getPrice());
    }
}
