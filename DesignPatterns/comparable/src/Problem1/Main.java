package Problem1;



import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class Main {

    public static void main(String [] arg){

        int[]a={4,5,30,100,200,15,400};
        solution(a);
        System.out.println(solution(a));

        }

        public static int solution(int[] A) {
        int p = 1;
            BiFunction<Integer,int [],Integer> separator=(x,a)-> {
                int sumx=0;
                for (int i = 0; i < x; i++) {
                    sumx=sumx+a[i];
                }
                int sumTotal = IntStream.of(a).sum();
                int r = Math.abs(sumTotal - (2*sumx));
                return r;
            };
            int min=Integer.MAX_VALUE;
        for (int i=0;i<A.length-1;i++){
            int r =separator.apply(p,A);
            if (r<min){min = r;}
            p=p+1;
        }
        return min;
    }

}