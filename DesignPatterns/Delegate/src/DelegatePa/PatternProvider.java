package DelegatePa;

import java.lang.reflect.Method;

public class PatternProvider  {



    String sChar = "";
    String sLines = "\n";
    PatternPProtocol deligate;

    public void getStuff (){

        char character = this.getChar();
        int lines = this.getNumLines();
        Pattern pattern = this.getPattern();
        String sC = this.getSpaceChar();
        String sL = this.getSpeaceLines();

        draw(character, lines, pattern, sL, sC);
    }

    private char getChar() {

        if (deligate!=null){
            char theChar = deligate.whichCharacter();
            return theChar;
        }
        return ' ';
    }
    private int getNumLines(){
        if (deligate!=null){
            int lines = deligate.numberOfLines();
            return lines;
        }
        return 0;
    }
    private Pattern getPattern(){
        if (deligate!=null){
            Pattern pat = deligate.whichPattern();
            return pat;
        }
        return Pattern.noPattern;
    }
    private String getSpeaceLines(){
       Method methodToFind = null;
        try {
            methodToFind = ServiceRequester.class.getMethod("whichSpeaceLine", (Class<?>[]) null);
        } catch (NoSuchMethodException | SecurityException e) {
            System.out.println("Default Line space");
        }
        if (!methodToFind.isDefault()){
            SpeaceLines x = deligate.whichSpeaceLine();

            switch (x) {
                case one: this.sLines = "\n";
                case zero: this.sLines = "";
            }
            return this.sLines;
        } else {return this.sLines;}

    }
    private String getSpaceChar(){
        Method methodToFind = null;

     try {
            methodToFind = ServiceRequester.class.getMethod("whichSpeachChar", (Class<?>[]) null);
        } catch (NoSuchMethodException | SecurityException e) {
            System.out.println("Default character space");
        }
        if (!methodToFind.isDefault()){
        SpeaceChar x = deligate.whichSpeachChar();
            switch (x){
                case zero: this.sChar = "";
                case one: this.sChar = " ";
                case two: this.sChar = "  ";

            }
     }
        return this.sChar;

        }


    private void draw(char character, int lines, Pattern pattern, String sLines, String sCharacter){

        switch (pattern){
            case triangule:
                if (lines!=0) {
                for (int i=0; i<lines;i++){
                System.out.print(character + sCharacter);
                }
                System.out.print(sLines);
                lines -= 1;
                draw( character,lines,pattern, sLines,sCharacter);
                break;
                } else {return;}
            case reverseTriangule:

                if (lines!=0) {
                    lines -= 1;
                    draw(character, lines, pattern, sLines, sCharacter);
                    lines += 1;
                    for (int i=0; i<lines;i++){
                        System.out.print(character + sCharacter);
                    }
                    System.out.print(sLines);
                }
                break;

            case noPattern: System.out.println("Sorry, I need a pattern in order to print"); break;
        }
    }

}
