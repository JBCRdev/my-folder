package DelegatePa;

import java.awt.font.TextHitInfo;

enum Pattern {
    triangule, reverseTriangule, noPattern
}


public class ServiceRequester implements PatternPProtocol {

    char character = 'J';
    int numberOflines = 6;
    Pattern pattern = Pattern.reverseTriangule;
    SpeaceLines sLines = SpeaceLines.one;
    SpeaceChar sChar = SpeaceChar.two;
    PatternProvider pP;

    public void setPatternProvider(PatternProvider pp){
        this.pP= pp;
    }

    @Override
    public char whichCharacter() {
        return this.character;
    }

    @Override
    public int numberOfLines() {
        return this.numberOflines;
    }

    @Override
    public Pattern whichPattern() {
        return this.pattern;
    }

    @Override
    public SpeaceChar whichSpeachChar() {
        return this.sChar;
    }


}
