package DelegatePa;

public interface PatternPProtocol {

     char whichCharacter();
     int numberOfLines();
     Pattern whichPattern();

     default SpeaceLines whichSpeaceLine() {
           return null;
     }

     default SpeaceChar whichSpeachChar(){
         return null;
     }
}
