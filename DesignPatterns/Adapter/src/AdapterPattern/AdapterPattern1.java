package AdapterPattern;

public class AdapterPattern1 {

    public static void main(String[] args) {


        Drawer drawer = new Drawer();

        drawer.addShape(new Circle());
        drawer.addShape(new Square());
        drawer.addShape(new Rectangle());
        drawer.addShape(new TriangleAdapter());
        drawer.addShape(new RhombusAdapter());

        drawer.draw();
        drawer.resize();
    }
}
