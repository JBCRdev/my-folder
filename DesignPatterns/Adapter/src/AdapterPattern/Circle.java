package AdapterPattern;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Printing Circle");
    }

    @Override
    public void resize() {
        System.out.println("Resizing Circle");
    }

    @Override
    public String description() {
        return "Circle Shape";
    }
}
