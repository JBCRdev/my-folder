package AdapterPattern;

public class Triangle implements GeometricShape {

    private  double a = 23;
    private  double b = 15.6;
    private  double c = 11.5;

    @Override
    public double area() {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    @Override
    public double perimeter() {
        return a + b + c;
    }

    @Override
    public void drawShape() {
        System.out.println("Printing Triangle whit area: " + area() + " and perimeter: " + perimeter());
    }
}
