package AdapterPattern;

public class RhombusAdapter extends Rhombus implements Shape {

    public RhombusAdapter() {
        super();
    }

    @Override
    public void draw() {
        super.drawShape();
    }

    @Override
    public void resize() {
        System.out.println("Rhombus can't be resize. Please create new one with required values.");
    }

    @Override
    public String description() {
        return "Rhombus object";
    }
}
