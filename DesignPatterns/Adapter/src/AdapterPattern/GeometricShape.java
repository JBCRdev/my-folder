package AdapterPattern;

public interface GeometricShape {

    double area();
    double perimeter();
    void drawShape();
}
