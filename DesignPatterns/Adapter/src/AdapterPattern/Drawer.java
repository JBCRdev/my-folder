package AdapterPattern;

import javax.swing.plaf.PanelUI;
import java.util.ArrayList;

public class Drawer {

    ArrayList<Shape> list = new ArrayList ();

    public Drawer(){

    }

    public void addShape(Shape shape){
        this.getList().add(shape);
    }

    public ArrayList<Shape> getList() {
        return list;
    }


    public void  draw(){
        if (getList().isEmpty()) {
            System.out.println("Nothing to draw!");
        } else {
            getList().stream().forEach(shape -> shape.draw());
        }
    }


    public void resize(){
        if (getList().isEmpty()) {
            System.out.println("Nothing to resize!");
        } else {
            getList().stream().forEach(shape -> shape.resize());
        }
    }
}
