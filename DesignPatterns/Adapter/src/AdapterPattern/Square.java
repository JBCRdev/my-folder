package AdapterPattern;

public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing square");
    }

    @Override
    public void resize() {
        System.out.println("Resizing square");
    }

    @Override
    public String description() {
        return "Square Shape";
    }
}
