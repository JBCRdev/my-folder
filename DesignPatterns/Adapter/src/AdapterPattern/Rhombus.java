package AdapterPattern;

public class Rhombus implements GeometricShape {

    double a = 14.5;
    double b = 23.4;
    @Override
    public double area() {

        double s = a * b;
        return s;

    }

    @Override
    public double perimeter() {
        return 2 * (a + b);
    }

    @Override
    public void drawShape() {
        System.out.println("Printing Rhombus whit area: " + area() + " and perimeter: " + perimeter());
    }
}
