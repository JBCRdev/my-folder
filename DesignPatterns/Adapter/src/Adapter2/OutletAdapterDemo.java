package Adapter2;

public class OutletAdapterDemo {

    public static void main(String[] args) {

        CAOutlet outlet = new CAOutlet();

        outlet.plugin(new UKAdapter());
        outlet.showType();
        outlet.showVoltage();
        outlet.showIsOn();
    }
}
