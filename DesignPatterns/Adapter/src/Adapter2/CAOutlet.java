package Adapter2;

public class CAOutlet {

    CanadaPlug plug;

    public  CAOutlet(){

    }

    void plugin(CanadaPlug newPlug){
        this.plug = newPlug;
    }

    void showType(){
        plug.plugType();
    }

    void showVoltage(){
        plug.voltage();
    }

    void showIsOn(){
        plug.isPlug();
    }
}
