package Adapter2;

public class UKAdapter extends UKPlugObject implements CanadaPlug {

    public UKAdapter(){
        super();
    }


    @Override
    public void plugType() {
        System.out.println("converted from: "+super.UKPlugType()+" to: "+ CAPlugObject.plugType() +".");
    }

    @Override
    public void voltage() {
        System.out.println("converted from: "+super.UKVoltage()+" Volts to: "+CAPlugObject.voltage()+ " Volts.");
    }

    @Override
    public void isPlug() {
        System.out.println("Is it plug? : "+CAPlugObject.isPlug()+".");
    }
}
