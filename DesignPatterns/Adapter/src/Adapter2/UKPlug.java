package Adapter2;

public interface UKPlug {

    Type UKPlugType();
    int UKVoltage();
}
