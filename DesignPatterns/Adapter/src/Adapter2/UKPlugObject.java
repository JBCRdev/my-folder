package Adapter2;


public class UKPlugObject implements UKPlug {


    public UKPlugObject(){

    }

    @Override
    public Type UKPlugType() {
        return Type.UK;
    }

    @Override
    public int UKVoltage() {
        return 230;
    }
}
