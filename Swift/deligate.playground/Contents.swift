import UIKit

enum Pattern {
    case triangule
    case reverseTriangule
}
/*
@objc protocol optionalFunctions: NSObjectProtocol {
    @objc optional var spaceChar: Int {get}
    @objc optional var spaceLines: Int {get}
    
    @objc optional func getSpaceChar()->Int
    @objc optional func getspaceLines()->Int
}
 */
 protocol PatternPDelegate: AnyObject, NSObjectProtocol{
    var char: Character {get}
    var lines: Int {get}
    var pattern : Pattern {get}
    var spaceChar: Int {get}
    var spaceLines: Int {get}
    
    func getCharacter()-> Character
    func getNumberOfLines()-> Int
    func getPattern()->Pattern
}

extension PatternPDelegate  {
    var spaceChar: Int {get{return 2}}
    var spaceLines: Int {get{return 1}}
    
    func getSpaceChar()->Int? {return nil}
    func getspaceLines()-> Int? {return nil}
}

class PatternProvider  {
    
    let sChar: String = " "
    let sLines: String = ""
    weak var delagate: PatternPDelegate?
    
    
    func getStuff(){
        
        let char = chooseCharacter()
        var lines = howmanyLines()
        let pattern = whichPattern()
        let speaceC = spaceChar()
        let speaceL = spaceLine()
        
        drow(char: char, lines: &lines, pattern: pattern, sC: speaceC, sL: speaceL)
}
    func chooseCharacter()->Character {
        let char = delagate?.getCharacter()
        return char!
    }
    func howmanyLines()->Int{
        let lines = delagate?.getNumberOfLines()
        
        return lines!
    }
    func whichPattern()->Pattern{
        let pattern = delagate?.getPattern()
        return pattern!
    }
    func spaceChar()->String{
        let spc: String
        
        if let space = delagate?.getSpaceChar() {
            
            switch space {
            case 0:
                spc = ""
            case 1:
                spc = " "
            default:
                spc = "  "
            }
            return spc
        }
        return sChar
    }
    
    func spaceLine()->String {
        let spl: String
        if let space = delagate?.getspaceLines(){
            switch space {
            case 0:
                spl = ""
            default:
                spl = "  "
            }
            return spl
        }
        return sLines
        }
    }
func drow(char: Character, lines: inout Int, pattern: Pattern, sC: String, sL: String){
        switch pattern {
        case .triangule:
            if lines != 0 {
                for _ in 1...lines{
                    print(char, terminator: sC)
                }
                print(sL)
                lines -= 1
                drow(char: char, lines: &lines, pattern: pattern, sC: sC, sL: sL)
            } else {return}
        default:
            if lines != 0{
                lines -= 1
                drow(char: char, lines: &lines, pattern: pattern, sC: sC, sL: sL)
                lines += 1
                for _ in 1...lines{
                    print(char, terminator: sC)
                }
                 print(sL)
            }
        }
    }

class ServiceRequester:  NSObject, PatternPDelegate {
    
    init(char: Character, NumberOfLines: Int, Pattern: Pattern) {
        
        self.char = char
        self.lines = NumberOfLines
        self.pattern = Pattern
        super.init()
        
    }
    var char: Character
    var lines: Int
    var pattern: Pattern
    var spaceChar: Int {get{return 2}}
    var patternProvider : PatternProvider?
    
    func setProvider() { self.patternProvider?.delagate = self}
    
    func getCharacter() -> Character {
        
        return self.char
    }
    
    func getNumberOfLines() -> Int {
        
        return self.lines
    }
    
    func getPattern() -> Pattern {
        
        return self.pattern
    }
    func getSpaceChar()->Int?{return spaceChar}
}

//Main

var serviceR = ServiceRequester(char: "J", NumberOfLines: 6, Pattern: .triangule)
var designer = PatternProvider()
serviceR.patternProvider = designer
serviceR.patternProvider?.delagate = serviceR
designer.getStuff()






