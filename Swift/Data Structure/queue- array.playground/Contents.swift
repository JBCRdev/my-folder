
/**
 - Title: Queue EXAMPLE (inmplementing  Array)
 
 - Author: JOSUE CABRERA
 - Date: September 2018
 - Complexity: O(N)
 */

/// - Remark: This Enum of type Error will help to catch some errors of empty Array
enum EmptyArray : Error{
    case noItemsInArray
}
/// - Class one: This class is for all the nodes of our Queue
struct Queue<T> {
    /// We'll implement Queue in Array
    var items = [T]()
    /// # enQueue function will add a new item to the array
    /// - Note: I use mutating is order to add to the struct Queue
    mutating func enQueue(newItem item: T){
        items.append(item)
        print("You add: \(item)")
    }
    /// # deQueue function will delete a new item to the array
    /// - Returns: This returns a generic type
    mutating func deQueue()-> T {
        /// This implements the catch of the error (EmptyArray)
        do {
            try isItEmpty()
        } catch EmptyArray.noItemsInArray {
            print("Sorry. Queue is Empty")
        } catch {print("An unexpected error occured")}
        ///- First: I take the member wich will be removed
        let r = items[0]
        print("You deleted \(r)")
        /// then remove from the array
        items.remove(at: 0)
        return r
    }
    /// Peek function
    /// - returns: This doesn't delete anything just returns the peek (fisrt in)
    func peek()-> T {
        do {
            try isItEmpty()
        } catch EmptyArray.noItemsInArray {
            print("Sorry. Queue is Empty")
        } catch {print("An unexpected error occured")}
        let r = items[0]
        print("The peek: \(r)")
        return r
    }
    /// Function to implement ErroHandling
    func isItEmpty()throws {
        guard items.isEmpty == false else {
            throw EmptyArray.noItemsInArray
        }
    }
}
///- Important: This extension will add som functionality to my Queue Struct but will not alterate the class itself
extension Queue {
    ///- Returns: Just returns the count of my array
    func size()-> Int { print("count: \(items.count)"); return items.count}
      /// Prints the items of the array
    func printQueue(){
       print(items)
    }
    /// This function works to create a instance witch inherit Vehicle class and enQueue it
    /// - Important: because of the imputs is specifically to objects which inherit Vehicle
   mutating func operator1 (brand: String, cS: Double, type: engine, fx:(String,Double,engine)->T)->T  {
        let newVheicle = fx(brand,cS,type)
        enQueue(newItem: newVheicle)
        print("A new Vahicle's been added")
        return newVheicle
    }
}
/// Creating class for an example
enum engine {
    case hybrid
    case gasoline
    case electric
}
class Vehicle {
    let brand : String
    var currentSpeed : Double
    /// - Note: the constructor alsa calls the parent constructor and assigns some values
    init(brand: String, currentSpeed: Double) {
        self.brand = brand
        self.currentSpeed = currentSpeed
    }
}
/// Car inherits from the class Vehicle
class Car: Vehicle {
    
    var engine: engine
    
    init(brand: String, engine: engine, cSpeed: Double) {
        self.engine = engine
        super.init(brand: brand, currentSpeed: cSpeed)
    }
}

///      MAIN   ////

/// We create and instance and change T for Int
var queue = Queue<Int>()
queue.enQueue(newItem: 23)
queue.enQueue(newItem: 34)
queue.enQueue(newItem: 33)
queue.deQueue()
queue.printQueue()


///- Experiment: Implementing operator function

/// We create and instance and change T for Vehicle
var vehicleQueue = Queue<Vehicle>()

/// Calling function operator 1-
/// Trailing closure for (name,cS,type)->Vehicle
vehicleQueue.operator1(brand: "Tesla", cS: 100.2, type: engine.electric) {
    (name,cS,type)->Vehicle in
    let newOne = Car(brand: name, engine: type, cSpeed: cS)
    return newOne
}

