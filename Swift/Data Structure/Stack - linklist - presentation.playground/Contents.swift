
/**
 - Title: Stack EXAMPLE (inmplementing  LinkList)
 
 - Author: JOSUE CABRERA
 - Date: September 2018
 - Complexity: O(1)
 */

/// - Remark: This Enum of type Error will help to catch some errors of empty LinkList
enum EmptyLinkList : Error {
    case noItems
}
/// - Class one: This class is for all the nodes of our LinkList
/// - Note: I couldn't use Struct here because  in Struc you Can't assign a property
///         of the same Struct type.
class Node<T> {
    /// Property for any value
    var value : T
    /// This is the pointer for the next node (The chain of the LinList)
    var hasnext : Node<T>? = nil
    /// Class constructor
    init(value: T) {
        self.value = value
    }
}
/// - Clas two: This is just for the LinkList
/// - Note: This is in another hand can be implement as struct
struct LinkList<T> {
    /// This property will always be my LinkList head
    var head : Node<T>?
    /** - Warning: Moving head can cause a lost of data in your Linklist. Rember, if Head change to another node
     you'll lose the node were Head used to be pointing
     */
    /// Computed property to get the first node(Head) easily
    /// - Note: I use Unwrapped node cause I have set my catch for Head = nil. Otherwise I should use optionel
    var firts: Node<T>! {
       
        /// Error Handling implementation
        do {
        try isItEmpty()
        } catch EmptyLinkList.noItems {print("Sorry. Stack is empty")}
        catch {print("An unexpected error occured")}
        return head
    }
    /// - Important: I Added mutating to all my struct functions to make changes in my properties
   mutating func addToList(value: T) {
    let newNode = Node(value: value)
    /// Error Handling implementation
    do {
        try isItEmpty()
        newNode.hasnext = head
        head = newNode
        /// if Head is nill creat a node an assign it to Head
    } catch EmptyLinkList.noItems {head = newNode}
    catch {print("An unexpected error occured")}
    
    }
    /**
     This function will help to delete some nodes. I'm adding to my Linklist from Head instead of tail
     - Returns: THIS RETURNS AN UNWRAPPED NODE BECAUSE OF THE ERROR HANDLING.
     */
    mutating func deleteFromList()-> Node<T> {
        /// Error Handling implementation
        do {
            try isItEmpty()
        } catch EmptyLinkList.noItems {print("Sorry. Stack is empty")}
        catch {print("An unexpected error occured")}
        if head?.hasnext != nil {
            let temp = head
            head = head?.hasnext
            temp?.hasnext = nil
            return temp!
        } else {
            let temp = head
            head = nil
            return temp!
        }
    }
    /// with this I just print my LinkList nodes
    mutating func printList()  {
        var temp = head
        while temp != nil {
            print(temp!.value)
            temp = temp!.hasnext
        }
    }
    ///- Remark: This function is for Error Handling
    ///- Throws: If the array is Empty ot not
    func isItEmpty() throws {
        guard head != nil else {
            throw EmptyLinkList.noItems
        }
    }
    
}


///- Important: CLASS STACK IMPLEMENTING LINKLIST
/**- Important: As you'll see in most of my Stack functions I call the LinkList class functions to add and remove
                so I just add print in some cases.
 */

struct Stack<T> {
    var list : LinkList<T>
    
    init(DataStruct list: LinkList<T>) {
        self.list = list
    }
    ///- Note: Push will put a new object/value into my Stack
    ///- Parameter value: it could be and object or a value
    mutating func push(value: T){
        list.addToList(value: value)
        print("You push this value: \(value)")
    }
    ///- Note: Pop will take an object/value from my Stack
    ///- Returns: A unwrapped Node. Which I use later on
    mutating func pop()-> Node<T> {
        let x = list.deleteFromList()
        print("You pop this value: \(x.value)")
        return x
    }
    /**- Note: Size function is the only one that has a time complexity of O(N).
                But is an optional function to implement.
      */
    mutating func size(){
        var count = 0
        var temp = list.head
        while temp != nil {
            count += 1
            temp = temp!.hasnext
        }
        print("Stack size: \(count)")
    }
    ///- Note: peek won't move, delete of add anything. This just returns the Peek of the Stack
   mutating  func peek(){
        let peek = list.firts!
        print("The peek of the Stack: \(peek.value)")
    }

}
/** #I implmeet extension as an example.
 
 - Note: Extension will help me to add two more funtions to the Stack but without change the main Class
 */
extension Stack{
    /// Just print not big deal
   mutating func printStack () {
        print("\nYour stack items are: \n")
        list.printList()
    }
   /**
     I am free to implement this function with many things
     - Parameter f: this is of funtion type (Node<T>)->E
     - returns: a generic Type E
     - Note: I'm using pop function wich will take the the last added object of my stack
     */
    mutating func operaator<E>(f: (Node<T>)->E)-> E {
        let r : E
        let t = pop()
            r = f(t)
        return r
        }
}
                        ///    # MAIN    //////

///
/// Instances for my LinkList and my Stack
 var list = LinkList<Int>()
 var stackObject = Stack(DataStruct: list)

stackObject.push(value: 10)
stackObject.push(value: 23)
stackObject.peek()
stackObject.pop()
stackObject.pop()

/// - Experiment: TASTING ERROR Handeling
//stackObject.pop()
stackObject.size()
//stackObject.peek()

stackObject.push(value: 43)
stackObject.push(value: 53)
stackObject.size()
stackObject.peek()
stackObject.printStack()


/// - Note: EXAMPLE OF  operaator()

stackObject.push(value: 234)
let str = stackObject.operaator(f: {(a: Node<Int>)->Double in return Double(a.value)})
print("\n OPERATOR RESULT: \(str)")




