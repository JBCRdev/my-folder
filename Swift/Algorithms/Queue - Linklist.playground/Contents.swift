//: Playground - noun: a place where people can play

import UIKit

/**
 - Title: Queue EXAMPLE (inmplementing  LinkList)
 
 - Author: JOSUE CABRERA
 - Date: September 2018
 - Complexity: O(1)
 */

/// - Remark: This Enum of type Error will help to catch some errors of empty LinkList
enum EmptyLinkList : Error {
    case noItems
}
/// - Class one: This class is for all the nodes of our LinkList
/// - Note: I couldn't use Struct here because  in Struc you Can't assign a property
///         of the same Struct type.
class Node<T> {
    /// Property for any value
    var value : T
    /// This is the pointer for the next node (The chain of the LinList)
    var hasnext : Node<T>? = nil
    /// Class constructor
    init(value: T) {
        self.value = value
    }
}
/// - Clas two: This is just for the LinkList
/// - Note: This is in another hand can be implement as struct
struct LinkList<T> {
    /// This property will always be my LinkList head
    var head : Node<T>?
    /** - Warning: Moving head can cause a lost of data in your Linklist. Rember, if Head change to another node
     you'll lose the node were Head used to be pointing
     */
    /// Computed property to get the first node(Head) easily
    /// - Note: I use Unwrapped node cause I have set my catch for Head = nil. Otherwise I should use optionel
   
    var firts: Node<T> {
   
        /// Error Handling implementation
        do {
            try isItEmpty()
        } catch EmptyLinkList.noItems {print("Sorry. Queue is empty")}
        catch {print("An unexpected error occured")}
        return head!
    }
    
    /// This property will always be my tail. It will help to add newObjects
    var tail:Node<T>?
    
    /// - Important: I Added mutating to almast all my struct functions to make changes in my properties
    mutating func addToList(value: T) {
        let newNode = Node(value: value)
        /// Error Handling implementation
        do {
            try isItEmpty()
            /// - Note: if the isItEmpty does not apply. We add new items from tail directly, without iterate from head to tail.
           tail?.hasnext = newNode
            tail = newNode
            /// if Head is nill creat a node an assign it to Head
        } catch EmptyLinkList.noItems {head = newNode; tail = newNode}
        catch {print("An unexpected error occured")}
        
    }
    /**
     This function will help to delete some nodes. I'm adding to my Linklist from Head instead of tail
     - Returns: THIS RETURNS AN UNWRAPPED NODE BECAUSE OF THE ERROR HANDLING.
     */
    mutating func deleteFromList()-> Node<T> {
        /// Error Handling implementation
        do {
            try isItEmpty()
        } catch EmptyLinkList.noItems {print("Sorry. Queue is empty")}
        catch {print("An unexpected error occured")}
        if head?.hasnext != nil {
            ///- Note: if head is not assign to another variable you'll move head
            let temp = head
            head = head?.hasnext
            temp?.hasnext = nil
            return temp!
        } else {
            let temp = head
            head = nil
            return temp!
        }
    }
    /// with this I just print my LinkList nodes
    mutating func printList()  {
        var temp = head
        while temp != nil {
            print(temp!.value)
            temp = temp!.hasnext
        }
    }
    ///- Remark: This function is for Error Handling
    ///- Throws: If the LinkList is Empty ot not
    func isItEmpty() throws {
        guard head != nil else {
            throw EmptyLinkList.noItems
        }
    }
}
/// Queue class of generic type
struct Queue<T> {
    var list = LinkList<T>()
    /// enQueue function will add an item to the list, calling addToList function from the linklist property
    mutating func enQueue(value: T) {
        list.addToList(value: value)
        print("You add a new value")
    }
    /// deQueue funtion deletes a node, the firt in out calling deleteFromList function from the linklist property
    mutating func deQueue()->Node<T> {
        let r = list.deleteFromList()
        print("You delete a value")
        return r
    }
    /// peek will just say the peek or head of the Linklist. This doesn't remove anything
    /// - Returns: the first node (Head)
    
    func peek()-> Node<T> {
        let peek = list.firts
        print("The peek is: \(peek.value)")
        return peek
    }
}
///- Important: This extension will add som functionality to my Queue Struct but will not alterate the class itself
extension Queue {
    ///- Returns: Just returns the count of my LinkList
    func size()-> Int {
        var temp = list.head
        var count = 0
        while temp != nil {
            count += 1
            temp = temp?.hasnext
        }
        print("The size: \(count)")
        return count
    }
    /// Prints the items of the Linklist
    func printQueue(){
        var temp = list.head
        while temp != nil {
            print(temp!.value)
            temp = temp?.hasnext
        }
    }
    /// This function works to create a instance witch inherit Vehicle class and enQueue it
    /// - Important: because of the imputs is specifically to objects which inherit Vehicle
    mutating func operator1 (brand: String, cS: Double, type: engine, fx:(String,Double,engine)->T)->T  {
        let newVheicle = fx(brand,cS,type)
        enQueue(value: newVheicle)
        print("A new Vahicle's been added")
        return newVheicle
    }
}
/// Creating class for an example
enum engine {
    case hybrid
    case gasoline
    case electric
}
class Vehicle {
    let brand : String
    var currentSpeed : Double
    /// - Note: the constructor alsa calls the parent constructor and assigns some values
    init(brand: String, currentSpeed: Double) {
        self.brand = brand
        self.currentSpeed = currentSpeed
    }
}
/// Car inherits from the class Vehicle
class Car: Vehicle {
    
    var engine: engine
    
    init(brand: String, engine: engine, cSpeed: Double) {
        self.engine = engine
        super.init(brand: brand, currentSpeed: cSpeed)
    }
}

///      MAIN   ////

/// We create and instance and change T for Int
var queue = Queue<Int>()
queue.enQueue(value: 23)
queue.enQueue(value: 34)
queue.enQueue(value: 33)
queue.deQueue()
queue.printQueue()

///- Experiment: Implementing operator function

/// We create and instance and change T for Vehicle
var vehicleQueue = Queue<Vehicle>()

/// Calling function operator 1-
/// Trailing closure for (name,cS,type)->Vehicle
vehicleQueue.operator1(brand: "Tesla", cS: 100.2, type: engine.electric) {
    (name,cS,type)->Vehicle in
    let newOne = Car(brand: name, engine: type, cSpeed: cS)
    return newOne
}
/*
/// - Experiment: creating an error
vehicleQueue.deQueue()
vehicleQueue.deQueue()
*/


