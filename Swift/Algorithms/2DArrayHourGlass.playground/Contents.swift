
import Foundation

//There are 16 hourglasses in arr , and an hourglass sum is the sum of an hourglass' values. Calculate the hourglass sum for every hourglass in arr , then print the maximum hourglass sum.

//https://www.hackerrank.com/challenges/2d-array/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays


let arr = [[1, 1, 1, 0, 0, 0],
           [0, 1, 0, 0, 0, 0],
           [1, 1, 1, 0, 0, 0],
           [0, 0, 2, 4, 4, 0],
           [0, 0, 0, 2, 0, 0],
           [0, 0, 1, 2, 4, 0]]



func hourglassSum(arr: [[Int]]) -> Int {
    let r = 6
    let c = 6
    var maxim = 0
    
    for i in 0...r-3{
        for j in 0...c-3 {
            
            let glassOne = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] +
                arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]
            
            maxim = max(maxim,glassOne)
        }
        
    }
    
    return maxim
}

print(hourglassSum(arr: arr))
