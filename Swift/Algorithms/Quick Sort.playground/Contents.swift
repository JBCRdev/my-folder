import UIKit

struct QuickSort {
    
    enum EmptyArray: Error {
        case empty
    }
    
    func sort(a: [Int])-> [Int] {
      /*
        do {
            try emptyArray(a: a)}
            catch EmptyArray.empty {print("sorry array is empty")}
        catch {"Unspected error"}
        */
            
            var greater = [Int]()
            var pivot = [Int]()
            var lower = [Int]()
            
            if a.count > 1{
                
                for index in 0...a.count-1 {
                    
                    switch a[index] {
                    case let number where number > a[0]:
                        greater.append(number)
                    case let number where number < a[0]:
                        lower.append(number)
                    default:
                        pivot.append(a[index])
                    }
                }
                return sort(a:lower) + pivot + sort(a: greater)
            }
            else if a.count < 1 {
                return a
            }
        return a
    }
    /*
    func emptyArray (a: [Int])throws {
        
        guard a.count != 0 else {
            throw EmptyArray.empty
        }
    }*/
    
}

//// Main ///
var list = [3,2,4,6,5,7,8,1,9,0]

let quick = QuickSort()
print(quick.sort(a: list))
